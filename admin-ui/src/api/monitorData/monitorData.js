import request from '@/utils/request'

// 查看是否有警告
export function listWarning(query) {
  return request({
    url: '/monitor/monitorData/listWarning',
    method: 'get',
    params: query
  })
}

// 折线图
export function countGroupByType(query) {
  return request({
    url: '/monitor/monitorData/countGroupByType',
    method: 'get',
    params: query
  })
}

// 查询注水管线数据列表
export function listArea(query) {
  return request({
    url: '/monitor/monitorData/listArea',
    method: 'get',
    params: query
  })
}

// 查询注水管线数据列表
export function listMonitorData(query) {
  return request({
    url: '/monitor/monitorData/list',
    method: 'get',
    params: query
  })
}

// 查询注水管线数据详细
export function getMonitorData(id) {
  return request({
    url: '/monitor/monitorData/' + id,
    method: 'get'
  })
}

// 新增注水管线数据
export function addMonitorData(data) {
  return request({
    url: '/monitor/monitorData',
    method: 'post',
    data: data
  })
}

// 修改注水管线数据
export function updateMonitorData(data) {
  return request({
    url: '/monitor/monitorData',
    method: 'put',
    data: data
  })
}

// 删除注水管线数据
export function delMonitorData(id) {
  return request({
    url: '/monitor/monitorData/' + id,
    method: 'delete'
  })
}
