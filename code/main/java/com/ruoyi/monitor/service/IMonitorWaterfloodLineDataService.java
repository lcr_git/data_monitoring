package com.ruoyi.monitor.service;

import java.util.List;
import com.ruoyi.monitor.domain.MonitorWaterfloodLineData;
import com.baomidou.mybatisplus.extension.service.IService;
/**
 * 注水管线数据Service接口
 * 
 * @author kf
 * @date 2024-03-16
 */
public interface IMonitorWaterfloodLineDataService extends IService<MonitorWaterfloodLineData>
{

}
