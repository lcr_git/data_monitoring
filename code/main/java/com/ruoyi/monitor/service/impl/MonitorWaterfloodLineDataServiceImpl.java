package com.ruoyi.monitor.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.monitor.mapper.MonitorWaterfloodLineDataMapper;
import com.ruoyi.monitor.domain.MonitorWaterfloodLineData;
import com.ruoyi.monitor.service.IMonitorWaterfloodLineDataService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * 注水管线数据Service业务层处理
 * 
 * @author kf
 * @date 2024-03-16
 */
@Service
public class MonitorWaterfloodLineDataServiceImpl  extends ServiceImpl<MonitorWaterfloodLineDataMapper, MonitorWaterfloodLineData> implements IMonitorWaterfloodLineDataService
{
    @Autowired
    private MonitorWaterfloodLineDataMapper monitorWaterfloodLineDataMapper;


}
