package com.ruoyi.monitor.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 注水管线数据对象 monitor_waterflood_line_data
 * 
 * @author kf
 * @date 2024-03-16
 */
@Data
public class MonitorWaterfloodLineData extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 温度值 */
    @Excel(name = "温度值")
    private String temperature;

    /** 压力 */
    @Excel(name = "压力")
    private String pressure;

    /** 流量 */
    @Excel(name = "流量")
    private String flow;

    /** 修改人 */
    @Excel(name = "修改人")
    private String uodateBy;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Long deleted;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTemperature(String temperature) 
    {
        this.temperature = temperature;
    }

    public String getTemperature() 
    {
        return temperature;
    }
    public void setPressure(String pressure) 
    {
        this.pressure = pressure;
    }

    public String getPressure() 
    {
        return pressure;
    }
    public void setFlow(String flow) 
    {
        this.flow = flow;
    }

    public String getFlow() 
    {
        return flow;
    }
    public void setUodateBy(String uodateBy) 
    {
        this.uodateBy = uodateBy;
    }

    public String getUodateBy() 
    {
        return uodateBy;
    }
    public void setDeleted(Long deleted) 
    {
        this.deleted = deleted;
    }

    public Long getDeleted() 
    {
        return deleted;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("temperature", getTemperature())
            .append("pressure", getPressure())
            .append("flow", getFlow())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("uodateBy", getUodateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("deleted", getDeleted())
            .toString();
    }
}
