-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('注水管线数据', '2000', '1', 'monitorData', 'monitor/monitorData/index', 1, 0, 'C', '0', '0', 'monitor:monitorData:list', '#', 'admin', sysdate(), '', null, '注水管线数据菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('注水管线数据查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'monitor:monitorData:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('注水管线数据新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'monitor:monitorData:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('注水管线数据修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'monitor:monitorData:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('注水管线数据删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'monitor:monitorData:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('注水管线数据导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'monitor:monitorData:export',       '#', 'admin', sysdate(), '', null, '');