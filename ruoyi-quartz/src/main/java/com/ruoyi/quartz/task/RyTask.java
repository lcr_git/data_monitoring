package com.ruoyi.quartz.task;

import com.alibaba.fastjson2.JSON;

import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.monitor.domain.MonitorWaterfloodLineData;
import com.ruoyi.monitor.mapper.MonitorWaterfloodLineDataMapper;
import com.ruoyi.monitor.service.IMonitorWaterfloodLineDataService;
import com.ruoyi.system.service.ISysDeptService;
import com.ruoyi.system.service.ISysUserService;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.ruoyi.common.utils.StringUtils;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 定时任务调度测试
 *
 * @author zhihuiwuliu
 */
@Component("ryTask")
public class RyTask {
    @Autowired
    private ISysDeptService sysDeptService;

    @Autowired
    private ISysUserService sysUserService;

    @Autowired
    private MonitorWaterfloodLineDataMapper monitorWaterfloodLineDataMapper;


    public void ryMultipleParams(String s, Boolean b, Long l, Double d, Integer i) {
        System.out.println(StringUtils.format("执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}", s, b, l, d, i));
    }

    public void ryParams(String params) {
        System.out.println("执行有参方法：" + params);
    }

    public void ryNoParams() {
        System.out.println("执行无参方法");
    }

    public void generateOrderData() {
        System.out.println("执行无参方法");
    }

    /**
     * 删除数据
     */
    public void deleteOrder() {
    }

    /**
     * 自动生成数据
     */
    public void generatedData() {

//        List<SysDept> sysDepts = sysDeptService.selectDeptList(new SysDept()).stream().filter(e -> e.getDeptId() != 100).collect(Collectors.toList());
//        int deptRandom = RandomUtils.nextInt(0, sysDepts.size());
//        monitorWaterfloodLineData.setDeptId(sysDepts.get(deptRandom).getDeptId());
//        List<SysDept> sysDept = sysDepts.stream().filter(e -> e.getDeptId() == monitorWaterfloodLineData.getDeptId()).collect(Collectors.toList());
//        monitorWaterfloodLineData.setDeptName(sysDept.get(0).getDeptName());
        MonitorWaterfloodLineData monitorWaterfloodLineData = new MonitorWaterfloodLineData();
        int temperature = RandomUtils.nextInt(1, 16);
        monitorWaterfloodLineData.setTemperature(Integer.toString(temperature));
        int pressure = RandomUtils.nextInt(1, 16);
        monitorWaterfloodLineData.setPressure(Integer.toString(pressure));
        int flow = RandomUtils.nextInt(1, 16);
        monitorWaterfloodLineData.setFlow(Integer.toString(flow));
        monitorWaterfloodLineData.setMaxValue(getMaxValue(temperature, pressure, flow));
        monitorWaterfloodLineData.setWarningMsg(getWarningMsg(temperature, "温度").append(getWarningMsg(pressure, "压力")).append(getWarningMsg(flow, "流量")).toString());

        List<SysUser> sysUsers = sysUserService.selectUserListAll(new SysUser()).stream().filter(e -> e.getDeptId() != 100).collect(Collectors.toList());
        int userRandom = RandomUtils.nextInt(0, sysUsers.size());
        SysUser sysUser = sysUsers.get(userRandom);
        monitorWaterfloodLineData.setCreateBy(sysUser.getUserName());
        monitorWaterfloodLineData.setDeptId(sysUser.getDeptId());
        SysDept sysDept = sysDeptService.selectDeptById(sysUser.getDeptId());
        monitorWaterfloodLineData.setDeptName(sysDept.getDeptName());
        monitorWaterfloodLineDataMapper.insert(monitorWaterfloodLineData);
    }

    /**
     * 获取最大值
     *
     * @return
     */
    private int getMaxValue(int v1, int v2, int v3) {
        Integer.toString(RandomUtils.nextInt(1, 16));
        int v = Math.max(v1, v2);
        return Math.max(v3, v);
    }

    /**
     * 获取警告语
     *
     * @return
     */
    private StringBuffer getWarningMsg(int value, String tagget) {
        // 提示
        StringBuffer sb = new StringBuffer();
        if (value > 10 && value < 16) {
            sb.append(tagget).append("一级错误。");
        } else if (value > 15) {
            sb.append(tagget).append("二级错误。");
        }
        return sb;
    }


    /**
     * 处理采购数据
     */
    public void processPurchasingData() {


    }


}
