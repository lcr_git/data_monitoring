//package com.ruoyi.quartz.config;
//
//import com.aliyun.openservices.lmq.example.util.ConnectionOptionWrapper;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.LinkedBlockingQueue;
//import java.util.concurrent.ThreadPoolExecutor;
//import java.util.concurrent.TimeUnit;
//import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
//import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
//import org.eclipse.paho.client.mqttv3.MqttClient;
//import org.eclipse.paho.client.mqttv3.MqttException;
//import org.eclipse.paho.client.mqttv3.MqttMessage;
//import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
//
//public class MQ4IoTSendMessageToMQ4IoTUseSignatureMode {
//    public static void main(String[] args) throws Exception {
//        /**
//         * 您在控制台创建的云消息队列 MQTT 版的实例ID。
//         */
//        String instanceId = "XXXXX";
//        /**
//         * 设置接入点，进入云消息队列 MQTT 版控制台实例详情页面获取。
//         */
//        String endPoint = "XXXXX.mqtt.aliyuncs.com";
//        /**
//         * AccessKey ID，阿里云身份验证，在阿里云RAM控制台创建。
//         * 阿里云账号AccessKey拥有所有API的访问权限，建议您使用RAM用户进行API访问或日常运维。
//         * 强烈建议不要把AccessKey ID和AccessKey Secret保存到工程代码里，否则可能导致AccessKey泄露，威胁您账号下所有资源的安全。
//         * 本示例以将AccessKey 和 AccessKeySecret 保存在环境变量为例说明。
//         */
//        String accessKey = System.getenv("MQTT_AK_ENV");
//        /**
//         * AccessKey Secret，阿里云身份验证，在阿里云RAM控制台创建。仅在签名鉴权模式下需要设置。
//         */
//        String secretKey = System.getenv("MQTT_SK_ENV");
//        /**
//         * MQTT客户端ID，由业务系统分配，需要保证每个TCP连接都不一样，保证全局唯一，如果不同的客户端对象（TCP连接）使用了相同的clientId会导致连接异常断开。
//         * clientId由两部分组成，格式为GroupID@@@DeviceID，其中GroupID在云消息队列 MQTT 版控制台创建，DeviceID由业务方自己设置，clientId总长度不得超过64个字符。
//         */
//        String clientId = "GID_XXXXX@@@XXXXX";
//        /**
//         * 云消息队列 MQTT 版消息的一级Topic，需要在控制台创建才能使用。
//         * 如果使用了没有创建或者没有被授权的Topic会导致鉴权失败，服务端会断开客户端连接。
//         */
//        final String parentTopic = "XXXXX";
//        /**
//         * 云消息队列 MQTT 版支持子级Topic，用来做自定义的过滤，此处为示例，可以填写任意字符串。
//         * 需要注意的是，完整的Topic长度不得超过128个字符。
//         */
//        final String mq4IotTopic = parentTopic + "/" + "testMq4Iot";
//        /**
//         * QoS参数代表传输质量，可选0，1，2。详细信息，请参见名词解释。
//         */
//        final int qosLevel = 0;
//        ConnectionOptionWrapper connectionOptionWrapper = new ConnectionOptionWrapper(instanceId, accessKey, secretKey, clientId);
//        final MemoryPersistence memoryPersistence = new MemoryPersistence();
//        /**
//         * 客户端协议和端口。客户端使用的协议和端口必须匹配，如果是SSL加密则设置ssl://endpoint:8883。
//         */
//        final MqttClient mqttClient = new MqttClient("tcp://" + endPoint + ":1883", clientId, memoryPersistence);
//        /**
//         * 设置客户端发送超时时间，防止无限阻塞。
//         */
//        mqttClient.setTimeToWait(5000);
//        final ExecutorService executorService = new ThreadPoolExecutor(1, 1, 0, TimeUnit.MILLISECONDS,
//            new LinkedBlockingQueue<Runnable>());
//        mqttClient.setCallback(new MqttCallbackExtended() {
//            @Override
//            public void connectComplete(boolean reconnect, String serverURI) {
//                /**
//                 * 客户端连接成功后就需要尽快订阅需要的Topic。
//                 */
//                System.out.println("connect success");
//                executorService.submit(new Runnable() {
//                    @Override
//                    public void run() {
//                        try {
//                            final String topicFilter[] = {mq4IotTopic};
//                            final int[] qos = {qosLevel};
//                            mqttClient.subscribe(topicFilter, qos);
//                        } catch (MqttException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                });
//            }
//
//            @Override
//            public void connectionLost(Throwable throwable) {
//                throwable.printStackTrace();
//            }
//
//            @Override
//            public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
//                /**
//                 * 消费消息的回调接口，需要确保该接口不抛异常，该接口运行返回即代表消息消费成功。
//                 * 消费消息需要保证在规定时间内完成，如果消费耗时超过服务端约定的超时时间，对于可靠传输的模式，服务端可能会重试推送，业务需要做好幂等去重处理。
//                 */
//                System.out.println(
//                    "receive msg from topic " + s + " , body is " + new String(mqttMessage.getPayload()));
//            }
//
//            @Override
//            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
//                System.out.println("send msg succeed topic is : " + iMqttDeliveryToken.getTopics()[0]);
//            }
//        });
//        mqttClient.connect(connectionOptionWrapper.getMqttConnectOptions());
//        for (int i = 0; i < 10; i++) {
//            MqttMessage message = new MqttMessage("hello mq4Iot pub sub msg".getBytes());
//            message.setQos(qosLevel);
//            /**
//             * 发送普通消息时，Topic必须和接收方订阅的Topic一致，或者符合通配符匹配规则。
//             */
//            mqttClient.publish(mq4IotTopic, message);
//            /**
//             * 云消息队列 MQTT 版支持点对点消息，即如果发送方明确知道该消息只需要给特定的一个设备接收，且知道对端的clientId，则可以直接发送点对点消息。
//             * 点对点消息不需要经过订阅关系匹配，可以简化订阅方的逻辑。点对点消息的Topic格式规范是 {{parentTopic}}/p2p/{{targetClientId}}。
//             */
//            final String p2pSendTopic = parentTopic + "/p2p/" + clientId;
//            message = new MqttMessage("hello mq4Iot p2p msg".getBytes());
//            message.setQos(qosLevel);
//            mqttClient.publish(p2pSendTopic, message);
//        }
//        Thread.sleep(Long.MAX_VALUE);
//    }
//}