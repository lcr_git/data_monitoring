/*
 Navicat Premium Data Transfer

 Source Server         : 本地宝塔
 Source Server Type    : MySQL
 Source Server Version : 50744
 Source Host           : 192.168.0.101:3306
 Source Schema         : monitor

 Target Server Type    : MySQL
 Target Server Version : 50744
 File Encoding         : 65001

 Date: 17/03/2024 00:02:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for QRTZ_BLOB_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_BLOB_TRIGGERS`;
CREATE TABLE `QRTZ_BLOB_TRIGGERS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `blob_data` blob NULL COMMENT '存放持久化Trigger对象',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `QRTZ_BLOB_TRIGGERS_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `QRTZ_TRIGGERS` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'Blob类型的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_BLOB_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_CALENDARS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_CALENDARS`;
CREATE TABLE `QRTZ_CALENDARS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '日历名称',
  `calendar` blob NOT NULL COMMENT '存放持久化calendar对象',
  PRIMARY KEY (`sched_name`, `calendar_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '日历信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_CALENDARS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_CRON_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_CRON_TRIGGERS`;
CREATE TABLE `QRTZ_CRON_TRIGGERS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `cron_expression` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'cron表达式',
  `time_zone_id` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '时区',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `QRTZ_CRON_TRIGGERS_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `QRTZ_TRIGGERS` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'Cron类型的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_CRON_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_FIRED_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_FIRED_TRIGGERS`;
CREATE TABLE `QRTZ_FIRED_TRIGGERS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `entry_id` varchar(95) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度器实例id',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度器实例名',
  `fired_time` bigint(13) NOT NULL COMMENT '触发的时间',
  `sched_time` bigint(13) NOT NULL COMMENT '定时器制定的时间',
  `priority` int(11) NOT NULL COMMENT '优先级',
  `state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '状态',
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '任务组名',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否并发',
  `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否接受恢复执行',
  PRIMARY KEY (`sched_name`, `entry_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '已触发的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_FIRED_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_JOB_DETAILS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_JOB_DETAILS`;
CREATE TABLE `QRTZ_JOB_DETAILS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务组名',
  `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '相关介绍',
  `job_class_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '执行任务类名称',
  `is_durable` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否持久化',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否并发',
  `is_update_data` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否更新数据',
  `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否接受恢复执行',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '任务详细信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_JOB_DETAILS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_LOCKS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_LOCKS`;
CREATE TABLE `QRTZ_LOCKS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `lock_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '悲观锁名称',
  PRIMARY KEY (`sched_name`, `lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '存储的悲观锁信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_LOCKS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_PAUSED_TRIGGER_GRPS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_PAUSED_TRIGGER_GRPS`;
CREATE TABLE `QRTZ_PAUSED_TRIGGER_GRPS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  PRIMARY KEY (`sched_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '暂停的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_PAUSED_TRIGGER_GRPS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_SCHEDULER_STATE
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SCHEDULER_STATE`;
CREATE TABLE `QRTZ_SCHEDULER_STATE`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '实例名称',
  `last_checkin_time` bigint(13) NOT NULL COMMENT '上次检查时间',
  `checkin_interval` bigint(13) NOT NULL COMMENT '检查间隔时间',
  PRIMARY KEY (`sched_name`, `instance_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '调度器状态表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_SCHEDULER_STATE
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_SIMPLE_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SIMPLE_TRIGGERS`;
CREATE TABLE `QRTZ_SIMPLE_TRIGGERS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `repeat_count` bigint(7) NOT NULL COMMENT '重复的次数统计',
  `repeat_interval` bigint(12) NOT NULL COMMENT '重复的间隔时间',
  `times_triggered` bigint(10) NOT NULL COMMENT '已经触发的次数',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `QRTZ_SIMPLE_TRIGGERS_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `QRTZ_TRIGGERS` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '简单触发器的信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_SIMPLE_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_SIMPROP_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SIMPROP_TRIGGERS`;
CREATE TABLE `QRTZ_SIMPROP_TRIGGERS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `str_prop_1` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第一个参数',
  `str_prop_2` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第二个参数',
  `str_prop_3` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第三个参数',
  `int_prop_1` int(11) NULL DEFAULT NULL COMMENT 'int类型的trigger的第一个参数',
  `int_prop_2` int(11) NULL DEFAULT NULL COMMENT 'int类型的trigger的第二个参数',
  `long_prop_1` bigint(20) NULL DEFAULT NULL COMMENT 'long类型的trigger的第一个参数',
  `long_prop_2` bigint(20) NULL DEFAULT NULL COMMENT 'long类型的trigger的第二个参数',
  `dec_prop_1` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第一个参数',
  `dec_prop_2` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第二个参数',
  `bool_prop_1` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第一个参数',
  `bool_prop_2` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第二个参数',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `QRTZ_SIMPROP_TRIGGERS_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `QRTZ_TRIGGERS` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '同步机制的行锁表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_SIMPROP_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_TRIGGERS`;
CREATE TABLE `QRTZ_TRIGGERS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '触发器的名字',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '触发器所属组的名字',
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_job_details表job_name的外键',
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_job_details表job_group的外键',
  `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '相关介绍',
  `next_fire_time` bigint(13) NULL DEFAULT NULL COMMENT '上一次触发时间（毫秒）',
  `prev_fire_time` bigint(13) NULL DEFAULT NULL COMMENT '下一次触发时间（默认为-1表示不触发）',
  `priority` int(11) NULL DEFAULT NULL COMMENT '优先级',
  `trigger_state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '触发器状态',
  `trigger_type` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '触发器的类型',
  `start_time` bigint(13) NOT NULL COMMENT '开始时间',
  `end_time` bigint(13) NULL DEFAULT NULL COMMENT '结束时间',
  `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '日程表名称',
  `misfire_instr` smallint(2) NULL DEFAULT NULL COMMENT '补偿执行的策略',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  INDEX `sched_name`(`sched_name`, `job_name`, `job_group`) USING BTREE,
  CONSTRAINT `QRTZ_TRIGGERS_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `QRTZ_JOB_DETAILS` (`sched_name`, `job_name`, `job_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '触发器详细信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (13, 'monitor_waterflood_line_data', '注水管线数据', NULL, NULL, 'MonitorWaterfloodLineData', 'crud', 'com.ruoyi.monitor', 'monitor', 'monitorData', '注水管线数据', 'kf', '0', '/', '{\"parentMenuId\":\"2000\"}', 'admin', '2024-03-16 19:03:48', '', '2024-03-16 19:42:16', NULL);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 138 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (125, '13', 'id', '主键', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2024-03-16 19:03:48', '', '2024-03-16 19:42:16');
INSERT INTO `gen_table_column` VALUES (126, '13', 'temperature', '温度值', 'int(6)', 'String', 'temperature', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', 'data_index', 2, 'admin', '2024-03-16 19:03:48', '', '2024-03-16 19:42:16');
INSERT INTO `gen_table_column` VALUES (127, '13', 'pressure', '压力', 'int(6)', 'String', 'pressure', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', 'data_index', 3, 'admin', '2024-03-16 19:03:48', '', '2024-03-16 19:42:16');
INSERT INTO `gen_table_column` VALUES (128, '13', 'flow', '流量', 'int(6)', 'String', 'flow', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', 'data_index', 4, 'admin', '2024-03-16 19:03:48', '', '2024-03-16 19:42:16');
INSERT INTO `gen_table_column` VALUES (129, '13', 'create_by', '创建人', 'varchar(20)', 'String', 'createBy', '0', '0', NULL, '0', NULL, '1', NULL, 'EQ', 'input', '', 8, 'admin', '2024-03-16 19:03:48', '', '2024-03-16 19:42:16');
INSERT INTO `gen_table_column` VALUES (130, '13', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '0', NULL, '1', NULL, 'EQ', 'datetime', '', 9, 'admin', '2024-03-16 19:03:48', '', '2024-03-16 19:42:16');
INSERT INTO `gen_table_column` VALUES (131, '13', 'uodate_by', '修改人', 'varchar(20)', 'String', 'uodateBy', '0', '0', NULL, '0', '0', '1', '0', 'EQ', 'input', '', 10, 'admin', '2024-03-16 19:03:48', '', '2024-03-16 19:42:16');
INSERT INTO `gen_table_column` VALUES (132, '13', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '0', '0', '1', NULL, 'EQ', 'datetime', '', 11, 'admin', '2024-03-16 19:03:48', '', '2024-03-16 19:42:16');
INSERT INTO `gen_table_column` VALUES (133, '13', 'remark', '备注', 'varchar(255)', 'String', 'remark', '0', '0', NULL, '0', '0', '0', NULL, 'EQ', 'input', '', 12, 'admin', '2024-03-16 19:03:48', '', '2024-03-16 19:42:16');
INSERT INTO `gen_table_column` VALUES (134, '13', 'deleted', '是否删除', 'bigint(20)', 'Long', 'deleted', '0', '0', NULL, '0', '0', '0', '0', 'EQ', 'input', '', 13, 'admin', '2024-03-16 19:03:48', '', '2024-03-16 19:42:16');
INSERT INTO `gen_table_column` VALUES (135, '13', 'dept_id', '所属区域id', 'bigint(20)', 'Long', 'deptId', '0', '0', NULL, '0', '0', '0', '0', 'EQ', 'input', '', 5, '', '2024-03-16 19:38:13', '', '2024-03-16 19:42:16');
INSERT INTO `gen_table_column` VALUES (136, '13', 'dept_name', '区域名称', 'varchar(20)', 'String', 'deptName', '0', '0', NULL, '1', '0', '1', '1', 'LIKE', 'input', '', 6, '', '2024-03-16 19:38:13', '', '2024-03-16 19:42:16');
INSERT INTO `gen_table_column` VALUES (137, '13', 'warning_msg', '警告提示', 'varchar(255)', 'String', 'warningMsg', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, '', '2024-03-16 19:38:13', '', '2024-03-16 19:42:16');

-- ----------------------------
-- Table structure for monitor_waterflood_line_data
-- ----------------------------
DROP TABLE IF EXISTS `monitor_waterflood_line_data`;
CREATE TABLE `monitor_waterflood_line_data`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `temperature` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '温度值',
  `pressure` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '压力',
  `flow` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '流量',
  `max_value` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最大值',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '所属区域id',
  `dept_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '区域名称',
  `warning_msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '警告提示',
  `create_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `deleted` bigint(20) NULL DEFAULT 0 COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1769030199896203267 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '注水管线数据' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of monitor_waterflood_line_data
-- ----------------------------
INSERT INTO `monitor_waterflood_line_data` VALUES (1769018031180197890, '11', '4', '12', '12', 102, 'B区域', '温度一级错误。流量一级错误。', 'testb', '2024-03-16 23:09:00', NULL, NULL, NULL, 0);
INSERT INTO `monitor_waterflood_line_data` VALUES (1769018785974562817, '3', '12', '9', '12', 101, 'A区域', '压力一级错误。', 'admin', '2024-03-16 23:12:00', NULL, NULL, NULL, 0);
INSERT INTO `monitor_waterflood_line_data` VALUES (1769019540924116993, '2', '10', '14', '14', 101, 'A区域', '流量一级错误。', 'admin', '2024-03-16 23:15:00', NULL, NULL, NULL, 0);
INSERT INTO `monitor_waterflood_line_data` VALUES (1769020295919808514, '2', '8', '9', '9', 101, 'A区域', '', 'testa', '2024-03-16 23:18:00', NULL, NULL, NULL, 0);
INSERT INTO `monitor_waterflood_line_data` VALUES (1769021050873556994, '9', '15', '6', '15', 102, 'B区域', '压力一级错误。', 'testb', '2024-03-16 23:21:00', NULL, NULL, NULL, 0);
INSERT INTO `monitor_waterflood_line_data` VALUES (1769021805881831425, '12', '3', '7', '12', 101, 'A区域', '温度一级错误。', 'admin', '2024-03-16 23:24:00', NULL, NULL, NULL, 0);
INSERT INTO `monitor_waterflood_line_data` VALUES (1769022560839774209, '6', '9', '1', '9', 101, 'A区域', '', 'testa', '2024-03-16 23:27:00', NULL, NULL, NULL, 0);
INSERT INTO `monitor_waterflood_line_data` VALUES (1769023315831271425, '2', '10', '5', '10', 101, 'A区域', '', 'admin', '2024-03-16 23:30:00', NULL, NULL, NULL, 0);
INSERT INTO `monitor_waterflood_line_data` VALUES (1769024070805991425, '3', '7', '15', '15', 101, 'A区域', '流量一级错误。', 'testa', '2024-03-16 23:33:00', NULL, NULL, NULL, 0);
INSERT INTO `monitor_waterflood_line_data` VALUES (1769024826053312513, '2', '5', '1', '5', 102, 'B区域', '', 'testb', '2024-03-16 23:36:00', NULL, NULL, NULL, 0);
INSERT INTO `monitor_waterflood_line_data` VALUES (1769025581040648194, '4', '2', '4', '4', 101, 'A区域', '', 'testa', '2024-03-16 23:39:00', NULL, NULL, NULL, 0);
INSERT INTO `monitor_waterflood_line_data` VALUES (1769026335763709953, '11', '7', '1', '11', 101, 'A区域', '温度一级错误。', 'testa', '2024-03-16 23:42:00', NULL, NULL, NULL, 0);
INSERT INTO `monitor_waterflood_line_data` VALUES (1769027090935447554, '7', '7', '13', '13', 101, 'A区域', '流量一级错误。', 'testa', '2024-03-16 23:45:00', NULL, NULL, NULL, 0);
INSERT INTO `monitor_waterflood_line_data` VALUES (1769027845662703618, '5', '9', '13', '13', 101, 'A区域', '流量一级错误。', 'admin', '2024-03-16 23:48:00', NULL, NULL, NULL, 0);
INSERT INTO `monitor_waterflood_line_data` VALUES (1769028600884912130, '3', '4', '6', '6', 102, 'B区域', '', 'testb', '2024-03-16 23:51:00', NULL, NULL, NULL, 0);
INSERT INTO `monitor_waterflood_line_data` VALUES (1769029355595390977, '9', '4', '1', '9', 102, 'B区域', '', 'testb', '2024-03-16 23:54:00', NULL, NULL, NULL, 0);
INSERT INTO `monitor_waterflood_line_data` VALUES (1769030110607859713, '10', '11', '11', '11', 101, 'A区域', '压力一级错误。流量一级错误。', 'admin', '2024-03-16 23:57:00', NULL, NULL, NULL, 0);
INSERT INTO `monitor_waterflood_line_data` VALUES (1769030199896203266, '1', '16', '10', '16', 101, 'A区域', '压力二级错误。', 'admin', '2024-03-16 23:57:21', NULL, NULL, NULL, 0);

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2024-03-06 09:31:49', 'admin', '2024-03-08 22:57:46', '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2024-03-06 09:31:49', '', NULL, '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-light', 'Y', 'admin', '2024-03-06 09:31:49', 'admin', '2024-03-08 22:57:12', '深色主题theme-dark，浅色主题theme-light');
INSERT INTO `sys_config` VALUES (4, '账号自助-验证码开关', 'sys.account.captchaEnabled', 'true', 'Y', 'admin', '2024-03-06 09:31:49', '', NULL, '是否开启验证码功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES (5, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'true', 'Y', 'admin', '2024-03-06 09:31:49', 'admin', '2024-03-11 22:03:22', '是否开启注册用户功能（true开启，false关闭）');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 103 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '智慧物流数据中心', 0, '管理员', '15888888888', 'guanliyuan@qq.com', '0', '0', 'admin', '2024-03-06 09:31:39', '', NULL);
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', 'A区域', 0, '张三', NULL, NULL, '0', '0', 'admin', '2024-03-16 19:25:25', '', NULL);
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', 'B区域', 1, NULL, NULL, NULL, '0', '0', 'admin', '2024-03-16 19:25:49', '', NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 127 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2024-03-06 09:31:48', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2024-03-06 09:31:48', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2024-03-06 09:31:48', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2024-03-06 09:31:48', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2024-03-06 09:31:48', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2024-03-06 09:31:48', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2024-03-06 09:31:48', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2024-03-06 09:31:48', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2024-03-06 09:31:48', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2024-03-06 09:31:48', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2024-03-06 09:31:48', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2024-03-06 09:31:48', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2024-03-06 09:31:48', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2024-03-06 09:31:48', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2024-03-06 09:31:48', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2024-03-06 09:31:48', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2024-03-06 09:31:48', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 99, '其他', '0', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2024-03-06 09:31:48', '', NULL, '其他操作');
INSERT INTO `sys_dict_data` VALUES (19, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2024-03-06 09:31:48', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (20, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2024-03-06 09:31:48', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (21, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2024-03-06 09:31:48', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (22, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2024-03-06 09:31:48', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (23, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2024-03-06 09:31:48', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (24, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2024-03-06 09:31:48', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (25, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2024-03-06 09:31:48', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (26, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2024-03-06 09:31:48', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (27, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2024-03-06 09:31:48', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (28, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2024-03-06 09:31:49', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (29, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2024-03-06 09:31:49', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (111, 0, '正常', '1', 'data_index', NULL, 'success', 'N', '0', 'admin', '2024-03-16 19:08:55', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (112, 0, '正常', '2', 'data_index', NULL, 'success', 'N', '0', 'admin', '2024-03-16 19:08:55', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (113, 0, '正常', '3', 'data_index', NULL, 'success', 'N', '0', 'admin', '2024-03-16 19:08:55', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (114, 0, '正常', '4', 'data_index', NULL, 'success', 'N', '0', 'admin', '2024-03-16 19:08:55', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (115, 0, '正常', '5', 'data_index', NULL, 'success', 'N', '0', 'admin', '2024-03-16 19:08:55', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (116, 0, '正常', '6', 'data_index', NULL, 'success', 'N', '0', 'admin', '2024-03-16 19:08:55', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (117, 0, '正常', '7', 'data_index', NULL, 'success', 'N', '0', 'admin', '2024-03-16 19:08:55', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (118, 0, '正常', '8', 'data_index', NULL, 'success', 'N', '0', 'admin', '2024-03-16 19:08:55', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (119, 0, '正常', '9', 'data_index', NULL, 'success', 'N', '0', 'admin', '2024-03-16 19:08:55', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (120, 0, '正常', '10', 'data_index', NULL, 'success', 'N', '0', 'admin', '2024-03-16 19:08:55', 'admin', '2024-03-16 23:32:28', NULL);
INSERT INTO `sys_dict_data` VALUES (121, 0, '一级错误', '11', 'data_index', NULL, 'warning', 'N', '0', 'admin', '2024-03-16 19:08:55', 'admin', '2024-03-16 19:12:30', NULL);
INSERT INTO `sys_dict_data` VALUES (122, 0, '一级错误', '12', 'data_index', NULL, 'warning', 'N', '0', 'admin', '2024-03-16 19:08:55', 'admin', '2024-03-16 19:12:34', NULL);
INSERT INTO `sys_dict_data` VALUES (123, 0, '一级错误', '13', 'data_index', NULL, 'warning', 'N', '0', 'admin', '2024-03-16 19:08:55', 'admin', '2024-03-16 19:12:39', NULL);
INSERT INTO `sys_dict_data` VALUES (124, 0, '一级错误', '14', 'data_index', NULL, 'warning', 'N', '0', 'admin', '2024-03-16 19:08:55', 'admin', '2024-03-16 19:12:47', NULL);
INSERT INTO `sys_dict_data` VALUES (125, 0, '一级错误', '15', 'data_index', NULL, 'warning', 'N', '0', 'admin', '2024-03-16 19:08:55', 'admin', '2024-03-16 19:12:52', NULL);
INSERT INTO `sys_dict_data` VALUES (126, 0, '二级错误', '16', 'data_index', NULL, 'danger', 'N', '0', 'admin', '2024-03-16 19:08:55', 'admin', '2024-03-16 19:12:57', NULL);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 105 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2024-03-06 09:31:47', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2024-03-06 09:31:47', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2024-03-06 09:31:47', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2024-03-06 09:31:47', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2024-03-06 09:31:47', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2024-03-06 09:31:47', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2024-03-06 09:31:47', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2024-03-06 09:31:47', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2024-03-06 09:31:47', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2024-03-06 09:31:48', '', NULL, '登录状态列表');
INSERT INTO `sys_dict_type` VALUES (104, '数据指标', 'data_index', '0', 'admin', '2024-03-16 19:08:15', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 103 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2024-03-06 09:31:49', '', NULL, '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2024-03-06 09:31:49', '', NULL, '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2024-03-06 09:31:49', '', NULL, '');
INSERT INTO `sys_job` VALUES (100, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '0 0/3 * * * ?', '1', '1', '0', 'admin', '2024-03-06 18:45:20', 'admin', '2024-03-16 23:09:32', '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 118 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------
INSERT INTO `sys_job_log` VALUES (1, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：9毫秒', '1', 'java.lang.reflect.InvocationTargetException\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at com.ruoyi.quartz.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:61)\r\n	at com.ruoyi.quartz.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:33)\r\n	at com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:19)\r\n	at com.ruoyi.quartz.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:43)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\nCaused by: com.ruoyi.common.exception.ServiceException: 获取用户信息异常\r\n	at com.ruoyi.common.utils.SecurityUtils.getLoginUser(SecurityUtils.java:73)\r\n	at com.ruoyi.framework.aspectj.DataScopeAspect.handleDataScope(DataScopeAspect.java:68)\r\n	at com.ruoyi.framework.aspectj.DataScopeAspect.doBefore(DataScopeAspect.java:62)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at org.springframework.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethodWithGivenArgs(AbstractAspectJAdvice.java:634)\r\n	at org.springframework.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethod(AbstractAspectJAdvice.java:617)\r\n	at org.springframework.aop.aspectj.AspectJMethodBeforeAdvice.before(AspectJMethodBeforeAdvice.java:44)\r\n	at org.springframework.aop.framework.adapter.MethodBeforeAdviceInterceptor.invoke(MethodBeforeAdviceInterceptor.java:57)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.j', '2024-03-16 21:17:49');
INSERT INTO `sys_job_log` VALUES (2, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：1毫秒', '1', 'java.lang.reflect.InvocationTargetException\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at com.ruoyi.quartz.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:61)\r\n	at com.ruoyi.quartz.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:33)\r\n	at com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:19)\r\n	at com.ruoyi.quartz.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:43)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\nCaused by: com.ruoyi.common.exception.ServiceException: 获取用户信息异常\r\n	at com.ruoyi.common.utils.SecurityUtils.getLoginUser(SecurityUtils.java:73)\r\n	at com.ruoyi.framework.aspectj.DataScopeAspect.handleDataScope(DataScopeAspect.java:68)\r\n	at com.ruoyi.framework.aspectj.DataScopeAspect.doBefore(DataScopeAspect.java:62)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at org.springframework.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethodWithGivenArgs(AbstractAspectJAdvice.java:634)\r\n	at org.springframework.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethod(AbstractAspectJAdvice.java:617)\r\n	at org.springframework.aop.aspectj.AspectJMethodBeforeAdvice.before(AspectJMethodBeforeAdvice.java:44)\r\n	at org.springframework.aop.framework.adapter.MethodBeforeAdviceInterceptor.invoke(MethodBeforeAdviceInterceptor.java:57)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.j', '2024-03-16 21:19:28');
INSERT INTO `sys_job_log` VALUES (3, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：8毫秒', '1', 'java.lang.reflect.InvocationTargetException\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at com.ruoyi.quartz.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:61)\r\n	at com.ruoyi.quartz.util.JobInvokeUtil.invokeMethod(JobInvokeUtil.java:33)\r\n	at com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution.doExecute(QuartzDisallowConcurrentExecution.java:19)\r\n	at com.ruoyi.quartz.util.AbstractQuartzJob.execute(AbstractQuartzJob.java:43)\r\n	at org.quartz.core.JobRunShell.run(JobRunShell.java:202)\r\n	at org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:573)\r\nCaused by: com.ruoyi.common.exception.ServiceException: 获取用户信息异常\r\n	at com.ruoyi.common.utils.SecurityUtils.getLoginUser(SecurityUtils.java:73)\r\n	at com.ruoyi.framework.aspectj.DataScopeAspect.handleDataScope(DataScopeAspect.java:68)\r\n	at com.ruoyi.framework.aspectj.DataScopeAspect.doBefore(DataScopeAspect.java:62)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:498)\r\n	at org.springframework.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethodWithGivenArgs(AbstractAspectJAdvice.java:634)\r\n	at org.springframework.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethod(AbstractAspectJAdvice.java:617)\r\n	at org.springframework.aop.aspectj.AspectJMethodBeforeAdvice.before(AspectJMethodBeforeAdvice.java:44)\r\n	at org.springframework.aop.framework.adapter.MethodBeforeAdviceInterceptor.invoke(MethodBeforeAdviceInterceptor.java:57)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.j', '2024-03-16 21:24:09');
INSERT INTO `sys_job_log` VALUES (4, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：65毫秒', '0', '', '2024-03-16 21:26:10');
INSERT INTO `sys_job_log` VALUES (5, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：25毫秒', '0', '', '2024-03-16 21:31:47');
INSERT INTO `sys_job_log` VALUES (6, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：9毫秒', '0', '', '2024-03-16 21:31:47');
INSERT INTO `sys_job_log` VALUES (7, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：11毫秒', '0', '', '2024-03-16 21:31:47');
INSERT INTO `sys_job_log` VALUES (8, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：11毫秒', '0', '', '2024-03-16 21:31:47');
INSERT INTO `sys_job_log` VALUES (9, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：11毫秒', '0', '', '2024-03-16 21:31:47');
INSERT INTO `sys_job_log` VALUES (10, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：11毫秒', '0', '', '2024-03-16 21:31:47');
INSERT INTO `sys_job_log` VALUES (11, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：40毫秒', '0', '', '2024-03-16 21:32:47');
INSERT INTO `sys_job_log` VALUES (12, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：37毫秒', '0', '', '2024-03-16 21:33:47');
INSERT INTO `sys_job_log` VALUES (13, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：31毫秒', '0', '', '2024-03-16 21:34:47');
INSERT INTO `sys_job_log` VALUES (14, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：32毫秒', '0', '', '2024-03-16 21:35:47');
INSERT INTO `sys_job_log` VALUES (15, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：30毫秒', '0', '', '2024-03-16 21:36:47');
INSERT INTO `sys_job_log` VALUES (16, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：30毫秒', '0', '', '2024-03-16 21:37:47');
INSERT INTO `sys_job_log` VALUES (17, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：30毫秒', '0', '', '2024-03-16 21:38:47');
INSERT INTO `sys_job_log` VALUES (18, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：30毫秒', '0', '', '2024-03-16 21:39:47');
INSERT INTO `sys_job_log` VALUES (19, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：69毫秒', '0', '', '2024-03-16 21:41:47');
INSERT INTO `sys_job_log` VALUES (20, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：14毫秒', '0', '', '2024-03-16 21:42:47');
INSERT INTO `sys_job_log` VALUES (21, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：33毫秒', '0', '', '2024-03-16 21:43:47');
INSERT INTO `sys_job_log` VALUES (22, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：37毫秒', '0', '', '2024-03-16 21:44:47');
INSERT INTO `sys_job_log` VALUES (23, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：32毫秒', '0', '', '2024-03-16 21:45:47');
INSERT INTO `sys_job_log` VALUES (24, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：36毫秒', '0', '', '2024-03-16 21:46:47');
INSERT INTO `sys_job_log` VALUES (25, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：46毫秒', '0', '', '2024-03-16 21:47:47');
INSERT INTO `sys_job_log` VALUES (26, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：34毫秒', '0', '', '2024-03-16 21:48:47');
INSERT INTO `sys_job_log` VALUES (27, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：31毫秒', '0', '', '2024-03-16 21:49:47');
INSERT INTO `sys_job_log` VALUES (28, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：21毫秒', '0', '', '2024-03-16 21:50:47');
INSERT INTO `sys_job_log` VALUES (29, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：10毫秒', '0', '', '2024-03-16 21:51:47');
INSERT INTO `sys_job_log` VALUES (30, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：32毫秒', '0', '', '2024-03-16 21:52:47');
INSERT INTO `sys_job_log` VALUES (31, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：48毫秒', '0', '', '2024-03-16 21:53:47');
INSERT INTO `sys_job_log` VALUES (32, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：41毫秒', '0', '', '2024-03-16 21:54:47');
INSERT INTO `sys_job_log` VALUES (33, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：31毫秒', '0', '', '2024-03-16 21:55:47');
INSERT INTO `sys_job_log` VALUES (34, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：32毫秒', '0', '', '2024-03-16 21:56:47');
INSERT INTO `sys_job_log` VALUES (35, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：33毫秒', '0', '', '2024-03-16 21:57:47');
INSERT INTO `sys_job_log` VALUES (36, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：30毫秒', '0', '', '2024-03-16 21:58:47');
INSERT INTO `sys_job_log` VALUES (37, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：30毫秒', '0', '', '2024-03-16 21:59:47');
INSERT INTO `sys_job_log` VALUES (38, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：42毫秒', '0', '', '2024-03-16 22:00:47');
INSERT INTO `sys_job_log` VALUES (39, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：30毫秒', '0', '', '2024-03-16 22:01:47');
INSERT INTO `sys_job_log` VALUES (40, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：32毫秒', '0', '', '2024-03-16 22:02:47');
INSERT INTO `sys_job_log` VALUES (41, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：32毫秒', '0', '', '2024-03-16 22:03:47');
INSERT INTO `sys_job_log` VALUES (42, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：49毫秒', '0', '', '2024-03-16 22:04:47');
INSERT INTO `sys_job_log` VALUES (43, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：41毫秒', '0', '', '2024-03-16 22:05:47');
INSERT INTO `sys_job_log` VALUES (44, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：30毫秒', '0', '', '2024-03-16 22:06:47');
INSERT INTO `sys_job_log` VALUES (45, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：29毫秒', '0', '', '2024-03-16 22:07:47');
INSERT INTO `sys_job_log` VALUES (46, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：35毫秒', '0', '', '2024-03-16 22:08:47');
INSERT INTO `sys_job_log` VALUES (47, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：39毫秒', '0', '', '2024-03-16 22:09:47');
INSERT INTO `sys_job_log` VALUES (48, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：31毫秒', '0', '', '2024-03-16 22:10:47');
INSERT INTO `sys_job_log` VALUES (49, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：34毫秒', '0', '', '2024-03-16 22:11:47');
INSERT INTO `sys_job_log` VALUES (50, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：86毫秒', '0', '', '2024-03-16 22:12:47');
INSERT INTO `sys_job_log` VALUES (51, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：47毫秒', '0', '', '2024-03-16 22:13:47');
INSERT INTO `sys_job_log` VALUES (52, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：35毫秒', '0', '', '2024-03-16 22:14:47');
INSERT INTO `sys_job_log` VALUES (53, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：37毫秒', '0', '', '2024-03-16 22:15:47');
INSERT INTO `sys_job_log` VALUES (54, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：51毫秒', '0', '', '2024-03-16 22:16:47');
INSERT INTO `sys_job_log` VALUES (55, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：36毫秒', '0', '', '2024-03-16 22:17:47');
INSERT INTO `sys_job_log` VALUES (56, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：39毫秒', '0', '', '2024-03-16 22:18:47');
INSERT INTO `sys_job_log` VALUES (57, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：35毫秒', '0', '', '2024-03-16 22:19:47');
INSERT INTO `sys_job_log` VALUES (58, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：31毫秒', '0', '', '2024-03-16 22:20:47');
INSERT INTO `sys_job_log` VALUES (59, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：35毫秒', '0', '', '2024-03-16 22:21:47');
INSERT INTO `sys_job_log` VALUES (60, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：55毫秒', '0', '', '2024-03-16 22:22:47');
INSERT INTO `sys_job_log` VALUES (61, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：31毫秒', '0', '', '2024-03-16 22:23:47');
INSERT INTO `sys_job_log` VALUES (62, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：12毫秒', '0', '', '2024-03-16 22:24:47');
INSERT INTO `sys_job_log` VALUES (63, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：24毫秒', '0', '', '2024-03-16 22:25:47');
INSERT INTO `sys_job_log` VALUES (64, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：15毫秒', '0', '', '2024-03-16 22:26:47');
INSERT INTO `sys_job_log` VALUES (65, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：13毫秒', '0', '', '2024-03-16 22:27:47');
INSERT INTO `sys_job_log` VALUES (66, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：47毫秒', '0', '', '2024-03-16 22:28:47');
INSERT INTO `sys_job_log` VALUES (67, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：12毫秒', '0', '', '2024-03-16 22:29:47');
INSERT INTO `sys_job_log` VALUES (68, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：11毫秒', '0', '', '2024-03-16 22:30:47');
INSERT INTO `sys_job_log` VALUES (69, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：24毫秒', '0', '', '2024-03-16 22:31:47');
INSERT INTO `sys_job_log` VALUES (70, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：33毫秒', '0', '', '2024-03-16 22:32:47');
INSERT INTO `sys_job_log` VALUES (71, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：35毫秒', '0', '', '2024-03-16 22:33:47');
INSERT INTO `sys_job_log` VALUES (72, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：15毫秒', '0', '', '2024-03-16 22:34:47');
INSERT INTO `sys_job_log` VALUES (73, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：34毫秒', '0', '', '2024-03-16 22:35:47');
INSERT INTO `sys_job_log` VALUES (74, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：32毫秒', '0', '', '2024-03-16 22:36:47');
INSERT INTO `sys_job_log` VALUES (75, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：31毫秒', '0', '', '2024-03-16 22:37:47');
INSERT INTO `sys_job_log` VALUES (76, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：32毫秒', '0', '', '2024-03-16 22:38:47');
INSERT INTO `sys_job_log` VALUES (77, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：50毫秒', '0', '', '2024-03-16 22:39:47');
INSERT INTO `sys_job_log` VALUES (78, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：39毫秒', '0', '', '2024-03-16 22:40:47');
INSERT INTO `sys_job_log` VALUES (79, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：49毫秒', '0', '', '2024-03-16 22:41:47');
INSERT INTO `sys_job_log` VALUES (80, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：51毫秒', '0', '', '2024-03-16 22:42:47');
INSERT INTO `sys_job_log` VALUES (81, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：52毫秒', '0', '', '2024-03-16 22:43:47');
INSERT INTO `sys_job_log` VALUES (82, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：46毫秒', '0', '', '2024-03-16 22:44:47');
INSERT INTO `sys_job_log` VALUES (83, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：35毫秒', '0', '', '2024-03-16 22:45:47');
INSERT INTO `sys_job_log` VALUES (84, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：49毫秒', '0', '', '2024-03-16 22:46:47');
INSERT INTO `sys_job_log` VALUES (85, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：36毫秒', '0', '', '2024-03-16 22:47:47');
INSERT INTO `sys_job_log` VALUES (86, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：47毫秒', '0', '', '2024-03-16 22:48:47');
INSERT INTO `sys_job_log` VALUES (87, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：29毫秒', '0', '', '2024-03-16 22:50:39');
INSERT INTO `sys_job_log` VALUES (88, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：45毫秒', '0', '', '2024-03-16 22:50:47');
INSERT INTO `sys_job_log` VALUES (89, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：77毫秒', '0', '', '2024-03-16 22:52:47');
INSERT INTO `sys_job_log` VALUES (90, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：45毫秒', '0', '', '2024-03-16 22:53:47');
INSERT INTO `sys_job_log` VALUES (91, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：37毫秒', '0', '', '2024-03-16 22:54:47');
INSERT INTO `sys_job_log` VALUES (92, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：79毫秒', '0', '', '2024-03-16 22:55:50');
INSERT INTO `sys_job_log` VALUES (93, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：41毫秒', '0', '', '2024-03-16 22:56:47');
INSERT INTO `sys_job_log` VALUES (94, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：40毫秒', '0', '', '2024-03-16 22:57:47');
INSERT INTO `sys_job_log` VALUES (95, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：36毫秒', '0', '', '2024-03-16 22:58:47');
INSERT INTO `sys_job_log` VALUES (96, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：35毫秒', '0', '', '2024-03-16 22:59:47');
INSERT INTO `sys_job_log` VALUES (97, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：68毫秒', '0', '', '2024-03-16 23:01:47');
INSERT INTO `sys_job_log` VALUES (98, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：66毫秒', '0', '', '2024-03-16 23:02:47');
INSERT INTO `sys_job_log` VALUES (99, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：41毫秒', '0', '', '2024-03-16 23:03:47');
INSERT INTO `sys_job_log` VALUES (100, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：63毫秒', '0', '', '2024-03-16 23:04:47');
INSERT INTO `sys_job_log` VALUES (101, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：56毫秒', '0', '', '2024-03-16 23:09:47');
INSERT INTO `sys_job_log` VALUES (102, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：35毫秒', '0', '', '2024-03-16 23:12:47');
INSERT INTO `sys_job_log` VALUES (103, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：31毫秒', '0', '', '2024-03-16 23:15:47');
INSERT INTO `sys_job_log` VALUES (104, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：35毫秒', '0', '', '2024-03-16 23:18:47');
INSERT INTO `sys_job_log` VALUES (105, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：31毫秒', '0', '', '2024-03-16 23:21:47');
INSERT INTO `sys_job_log` VALUES (106, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：38毫秒', '0', '', '2024-03-16 23:24:47');
INSERT INTO `sys_job_log` VALUES (107, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：33毫秒', '0', '', '2024-03-16 23:27:47');
INSERT INTO `sys_job_log` VALUES (108, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：37毫秒', '0', '', '2024-03-16 23:30:47');
INSERT INTO `sys_job_log` VALUES (109, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：46毫秒', '0', '', '2024-03-16 23:33:47');
INSERT INTO `sys_job_log` VALUES (110, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：47毫秒', '0', '', '2024-03-16 23:36:47');
INSERT INTO `sys_job_log` VALUES (111, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：62毫秒', '0', '', '2024-03-16 23:39:47');
INSERT INTO `sys_job_log` VALUES (112, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：46毫秒', '0', '', '2024-03-16 23:42:47');
INSERT INTO `sys_job_log` VALUES (113, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：81毫秒', '0', '', '2024-03-16 23:45:47');
INSERT INTO `sys_job_log` VALUES (114, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：38毫秒', '0', '', '2024-03-16 23:48:47');
INSERT INTO `sys_job_log` VALUES (115, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：72毫秒', '0', '', '2024-03-16 23:51:47');
INSERT INTO `sys_job_log` VALUES (116, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：35毫秒', '0', '', '2024-03-16 23:54:47');
INSERT INTO `sys_job_log` VALUES (117, '生成数据', 'SYSTEM', 'ryTask.generatedData()', '生成数据 总共耗时：27毫秒', '0', '', '2024-03-16 23:57:47');

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 145 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES (100, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-03-06 18:43:12');
INSERT INTO `sys_logininfor` VALUES (101, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-03-07 16:18:10');
INSERT INTO `sys_logininfor` VALUES (102, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-03-07 20:49:35');
INSERT INTO `sys_logininfor` VALUES (103, 'admin', '112.49.232.174', 'XX XX', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-07 21:13:14');
INSERT INTO `sys_logininfor` VALUES (104, 'admin', '112.49.232.174', 'XX XX', 'Chrome 12', 'Windows 10', '0', '退出成功', '2024-03-07 21:26:31');
INSERT INTO `sys_logininfor` VALUES (105, 'admin', '112.49.232.174', 'XX XX', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-07 21:27:31');
INSERT INTO `sys_logininfor` VALUES (106, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '1', '验证码错误', '2024-03-08 17:13:56');
INSERT INTO `sys_logininfor` VALUES (107, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-03-08 17:14:00');
INSERT INTO `sys_logininfor` VALUES (108, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-03-08 19:20:55');
INSERT INTO `sys_logininfor` VALUES (109, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-03-08 21:02:48');
INSERT INTO `sys_logininfor` VALUES (110, 'admin', '112.49.232.174', 'XX XX', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-08 21:19:14');
INSERT INTO `sys_logininfor` VALUES (111, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-03-08 21:22:41');
INSERT INTO `sys_logininfor` VALUES (112, 'admin', '112.49.232.174', 'XX XX', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-08 22:39:02');
INSERT INTO `sys_logininfor` VALUES (113, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-03-08 22:53:07');
INSERT INTO `sys_logininfor` VALUES (114, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-03-09 14:04:46');
INSERT INTO `sys_logininfor` VALUES (115, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-03-09 15:08:03');
INSERT INTO `sys_logininfor` VALUES (116, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-03-09 16:08:27');
INSERT INTO `sys_logininfor` VALUES (117, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '退出成功', '2024-03-09 17:18:56');
INSERT INTO `sys_logininfor` VALUES (118, 'admin', '112.5.171.128', 'XX XX', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-03-09 17:33:48');
INSERT INTO `sys_logininfor` VALUES (119, 'admin', '112.5.171.128', 'XX XX', 'Firefox 12', 'Windows 10', '0', '退出成功', '2024-03-09 17:55:00');
INSERT INTO `sys_logininfor` VALUES (120, 'admin', '112.5.171.128', 'XX XX', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-03-09 17:55:20');
INSERT INTO `sys_logininfor` VALUES (121, 'admin', '112.5.171.128', 'XX XX', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-03-09 22:40:36');
INSERT INTO `sys_logininfor` VALUES (122, 'admin', '211.143.179.145', 'XX XX', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-03-10 17:13:41');
INSERT INTO `sys_logininfor` VALUES (123, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-03-10 17:19:34');
INSERT INTO `sys_logininfor` VALUES (124, 'admin', '183.9.109.251', 'XX XX', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-11 10:35:43');
INSERT INTO `sys_logininfor` VALUES (125, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-11 11:09:00');
INSERT INTO `sys_logininfor` VALUES (126, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-11 16:53:40');
INSERT INTO `sys_logininfor` VALUES (127, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-11 20:39:11');
INSERT INTO `sys_logininfor` VALUES (128, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '0', '退出成功', '2024-03-11 20:55:42');
INSERT INTO `sys_logininfor` VALUES (129, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-11 20:55:47');
INSERT INTO `sys_logininfor` VALUES (130, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-11 21:39:06');
INSERT INTO `sys_logininfor` VALUES (131, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '0', '退出成功', '2024-03-11 21:40:38');
INSERT INTO `sys_logininfor` VALUES (132, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-11 21:42:19');
INSERT INTO `sys_logininfor` VALUES (133, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '0', '退出成功', '2024-03-11 22:03:33');
INSERT INTO `sys_logininfor` VALUES (134, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-11 22:03:38');
INSERT INTO `sys_logininfor` VALUES (135, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '0', '退出成功', '2024-03-11 22:04:16');
INSERT INTO `sys_logininfor` VALUES (136, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-03-16 18:56:25');
INSERT INTO `sys_logininfor` VALUES (137, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '退出成功', '2024-03-16 20:03:33');
INSERT INTO `sys_logininfor` VALUES (138, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-03-16 20:03:39');
INSERT INTO `sys_logininfor` VALUES (139, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '1', '验证码错误', '2024-03-16 22:15:26');
INSERT INTO `sys_logininfor` VALUES (140, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-03-16 22:15:30');
INSERT INTO `sys_logininfor` VALUES (141, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '1', '验证码错误', '2024-03-16 22:48:37');
INSERT INTO `sys_logininfor` VALUES (142, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-03-16 22:48:42');
INSERT INTO `sys_logininfor` VALUES (143, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '退出成功', '2024-03-16 23:54:14');
INSERT INTO `sys_logininfor` VALUES (144, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-03-16 23:54:21');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路由参数',
  `is_frame` int(1) NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int(1) NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2049 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 1, 'system', NULL, '', 1, 0, 'M', '0', '0', '', 'system', 'admin', '2024-03-06 09:31:40', '', NULL, '系统管理目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 3, 'tool', NULL, '', 1, 0, 'M', '0', '1', '', 'tool', 'admin', '2024-03-06 09:31:40', 'admin', '2024-03-16 23:52:37', '系统工具目录');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', '', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2024-03-06 09:31:41', '', NULL, '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', '', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2024-03-06 09:31:41', '', NULL, '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', '', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2024-03-06 09:31:41', '', NULL, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', '', 1, 0, 'C', '0', '0', 'system:dept:list', 'tree', 'admin', '2024-03-06 09:31:41', '', NULL, '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', '', 1, 0, 'C', '0', '1', 'system:post:list', 'post', 'admin', '2024-03-06 09:31:41', 'admin', '2024-03-16 23:52:52', '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', '', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2024-03-06 09:31:41', '', NULL, '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', '', 1, 0, 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2024-03-06 09:31:41', '', NULL, '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', '', 1, 0, 'C', '1', '1', 'system:notice:list', 'message', 'admin', '2024-03-06 09:31:41', 'admin', '2024-03-08 21:36:54', '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 2000, 9, 'log', '', '', 1, 0, 'M', '0', '1', '', 'log', 'admin', '2024-03-06 09:31:41', 'admin', '2024-03-16 23:53:23', '日志管理菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2000, 2, 'job', 'monitor/job/index', '', 1, 0, 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2024-03-06 09:31:41', 'admin', '2024-03-08 21:34:40', '定时任务菜单');
INSERT INTO `sys_menu` VALUES (115, '表单构建', 3, 1, 'build', 'tool/build/index', '', 1, 0, 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2024-03-06 09:31:41', '', NULL, '表单构建菜单');
INSERT INTO `sys_menu` VALUES (116, '代码生成', 3, 2, 'gen', 'tool/gen/index', '', 1, 0, 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2024-03-06 09:31:41', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu` VALUES (117, '系统接口', 3, 3, 'swagger', 'tool/swagger/index', '', 1, 0, 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2024-03-06 09:31:41', '', NULL, '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, 'operlog', 'monitor/operlog/index', '', 1, 0, 'C', '0', '0', 'monitor:operlog:list', 'form', 'admin', '2024-03-06 09:31:41', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, 'logininfor', 'monitor/logininfor/index', '', 1, 0, 'C', '0', '0', 'monitor:logininfor:list', 'logininfor', 'admin', '2024-03-06 09:31:41', '', NULL, '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1000, '用户查询', 100, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2024-03-06 09:31:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1001, '用户新增', 100, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2024-03-06 09:31:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户修改', 100, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2024-03-06 09:31:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户删除', 100, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2024-03-06 09:31:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '用户导出', 100, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2024-03-06 09:31:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1005, '用户导入', 100, 6, '', '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2024-03-06 09:31:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '重置密码', 100, 7, '', '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2024-03-06 09:31:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '角色查询', 101, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2024-03-06 09:31:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '角色新增', 101, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1009, '角色修改', 101, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '角色删除', 101, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '角色导出', 101, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '菜单查询', 102, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1013, '菜单新增', 102, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1014, '菜单修改', 102, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1015, '菜单删除', 102, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '部门查询', 103, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1017, '部门新增', 103, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '部门修改', 103, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1019, '部门删除', 103, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '岗位查询', 104, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:post:query', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1021, '岗位新增', 104, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:post:add', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1022, '岗位修改', 104, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:post:edit', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1023, '岗位删除', 104, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:post:remove', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1024, '岗位导出', 104, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:post:export', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1025, '字典查询', 105, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:query', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1026, '字典新增', 105, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:add', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1027, '字典修改', 105, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1028, '字典删除', 105, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1029, '字典导出', 105, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:export', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1030, '参数查询', 106, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:query', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1031, '参数新增', 106, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:add', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1032, '参数修改', 106, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:edit', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1033, '参数删除', 106, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:remove', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1034, '参数导出', 106, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:export', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1035, '公告查询', 107, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1036, '公告新增', 107, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1037, '公告修改', 107, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1038, '公告删除', 107, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2024-03-06 09:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1039, '操作查询', 500, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:query', '#', 'admin', '2024-03-06 09:31:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1040, '操作删除', 500, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:remove', '#', 'admin', '2024-03-06 09:31:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1041, '日志导出', 500, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:export', '#', 'admin', '2024-03-06 09:31:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1042, '登录查询', 501, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:query', '#', 'admin', '2024-03-06 09:31:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1043, '登录删除', 501, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:remove', '#', 'admin', '2024-03-06 09:31:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1044, '日志导出', 501, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:export', '#', 'admin', '2024-03-06 09:31:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1045, '账户解锁', 501, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:unlock', '#', 'admin', '2024-03-06 09:31:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1049, '任务查询', 110, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2024-03-06 09:31:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1050, '任务新增', 110, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2024-03-06 09:31:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1051, '任务修改', 110, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2024-03-06 09:31:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1052, '任务删除', 110, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2024-03-06 09:31:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1053, '状态修改', 110, 5, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2024-03-06 09:31:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1054, '任务导出', 110, 6, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2024-03-06 09:31:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1055, '生成查询', 116, 1, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2024-03-06 09:31:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1056, '生成修改', 116, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2024-03-06 09:31:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1057, '生成删除', 116, 3, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2024-03-06 09:31:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1058, '导入代码', 116, 4, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2024-03-06 09:31:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1059, '预览代码', 116, 5, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2024-03-06 09:31:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1060, '生成代码', 116, 6, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2024-03-06 09:31:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2000, '数据监测管理', 0, 0, 'monitorData', NULL, NULL, 1, 0, 'M', '0', '0', '', 'monitor', 'admin', '2024-03-07 16:29:18', 'admin', '2024-03-16 19:20:45', '');
INSERT INTO `sys_menu` VALUES (2037, '整体数据观测模块', 2000, 1, 'allMonitorData', 'monitorData/monitorData/index', NULL, 1, 0, 'C', '0', '0', 'monitor:monitorData:list', '#', 'admin', '2024-03-16 19:15:26', 'admin', '2024-03-16 19:32:53', '注水管线数据菜单');
INSERT INTO `sys_menu` VALUES (2038, '注水管线数据查询', 2037, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'monitor:monitorData:query', '#', 'admin', '2024-03-16 19:15:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2039, '注水管线数据新增', 2037, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'monitor:monitorData:add', '#', 'admin', '2024-03-16 19:15:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2040, '注水管线数据修改', 2037, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'monitor:monitorData:edit', '#', 'admin', '2024-03-16 19:15:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2041, '注水管线数据删除', 2037, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'monitor:monitorData:remove', '#', 'admin', '2024-03-16 19:15:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2042, '注水管线数据导出', 2037, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'monitor:monitorData:export', '#', 'admin', '2024-03-16 19:15:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2043, '报警数据观测模块', 2000, 1, 'areaMonitorData', 'monitorData/thisMonitorData/index', NULL, 1, 0, 'C', '0', '0', 'monitor:monitorData:list', '#', 'admin', '2024-03-16 19:15:26', 'admin', '2024-03-16 20:54:05', '注水管线数据菜单');
INSERT INTO `sys_menu` VALUES (2044, '注水管线数据查询', 2043, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'monitor:monitorData:query', '#', 'admin', '2024-03-16 19:15:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2045, '注水管线数据新增', 2043, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'monitor:monitorData:add', '#', 'admin', '2024-03-16 19:15:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2046, '注水管线数据修改', 2043, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'monitor:monitorData:edit', '#', 'admin', '2024-03-16 19:15:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2047, '注水管线数据删除', 2043, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'monitor:monitorData:remove', '#', 'admin', '2024-03-16 19:15:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2048, '注水管线数据导出', 2043, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'monitor:monitorData:export', '#', 'admin', '2024-03-16 19:15:26', '', NULL, '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '通知公告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '温馨提醒：2018-07-01 若依新版本发布啦', '2', 0xE696B0E78988E69CACE58685E5AEB9, '0', 'admin', '2024-03-06 09:31:49', '', NULL, '管理员');
INSERT INTO `sys_notice` VALUES (2, '维护通知：2018-07-01 若依系统凌晨维护', '1', 0xE7BBB4E68AA4E58685E5AEB9, '0', 'admin', '2024-03-06 09:31:49', '', NULL, '管理员');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 63 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (1, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"wms/lineMonitor/index\",\"createTime\":\"2024-03-07 16:54:40\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2031,\"menuName\":\"数据监测管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2000,\"path\":\"lineMonitor\",\"perms\":\"wms:lineMonitor:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:00:50');
INSERT INTO `sys_oper_log` VALUES (2, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2024-03-07 16:29:18\",\"icon\":\"monitor\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2000,\"menuName\":\"数据监测管理\",\"menuType\":\"M\",\"orderNum\":0,\"params\":{},\"parentId\":0,\"path\":\"wms\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:00:58');
INSERT INTO `sys_oper_log` VALUES (3, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2024-03-07 16:29:18\",\"icon\":\"monitor\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2000,\"menuName\":\"数据监测管理\",\"menuType\":\"M\",\"orderNum\":0,\"params\":{},\"parentId\":0,\"path\":\"lineMonitor\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:01:05');
INSERT INTO `sys_oper_log` VALUES (4, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"lineMonitor/data/index\",\"createTime\":\"2024-03-07 16:54:40\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2031,\"menuName\":\"数据监测管理\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2000,\"path\":\"data\",\"perms\":\"lineMonitor:data:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:01:43');
INSERT INTO `sys_oper_log` VALUES (5, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/2031', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"存在子菜单,不允许删除\",\"code\":601}', 0, NULL, '2024-03-16 19:01:52');
INSERT INTO `sys_oper_log` VALUES (6, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2024-03-06 09:31:40\",\"icon\":\"tool\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":3,\"menuName\":\"系统工具\",\"menuType\":\"M\",\"orderNum\":3,\"params\":{},\"parentId\":0,\"path\":\"tool\",\"perms\":\"\",\"query\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:03:21');
INSERT INTO `sys_oper_log` VALUES (7, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', '\"monitor_waterflood_line_data\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:03:48');
INSERT INTO `sys_oper_log` VALUES (8, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/monitor_waterflood_line_data', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:04:35');
INSERT INTO `sys_oper_log` VALUES (9, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"monitorData\",\"className\":\"MonitorWaterfloodLineData\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"主键\",\"columnId\":125,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2024-03-16 19:03:48\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":13,\"updateBy\":\"\",\"updateTime\":\"2024-03-16 19:04:35\",\"usableColumn\":false},{\"capJavaField\":\"Temperature\",\"columnComment\":\"温度值\",\"columnId\":126,\"columnName\":\"temperature\",\"columnType\":\"int(6)\",\"createBy\":\"admin\",\"createTime\":\"2024-03-16 19:03:48\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"temperature\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":13,\"updateBy\":\"\",\"updateTime\":\"2024-03-16 19:04:35\",\"usableColumn\":false},{\"capJavaField\":\"Pressure\",\"columnComment\":\"压力\",\"columnId\":127,\"columnName\":\"pressure\",\"columnType\":\"int(6)\",\"createBy\":\"admin\",\"createTime\":\"2024-03-16 19:03:48\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"pressure\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":13,\"updateBy\":\"\",\"updateTime\":\"2024-03-16 19:04:35\",\"usableColumn\":false},{\"capJavaField\":\"Flow\",\"columnComment\":\"流量\",\"columnId\":128,\"columnName\":\"flow\",\"columnType\":\"int(6)\",\"createBy\":\"admin\",\"createTime\":\"2024-03-16 19:03:48\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:05:14');
INSERT INTO `sys_oper_log` VALUES (10, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"monitorData\",\"className\":\"MonitorWaterfloodLineData\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"主键\",\"columnId\":125,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2024-03-16 19:03:48\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":13,\"updateBy\":\"\",\"updateTime\":\"2024-03-16 19:05:14\",\"usableColumn\":false},{\"capJavaField\":\"Temperature\",\"columnComment\":\"温度值\",\"columnId\":126,\"columnName\":\"temperature\",\"columnType\":\"int(6)\",\"createBy\":\"admin\",\"createTime\":\"2024-03-16 19:03:48\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"temperature\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":13,\"updateBy\":\"\",\"updateTime\":\"2024-03-16 19:05:14\",\"usableColumn\":false},{\"capJavaField\":\"Pressure\",\"columnComment\":\"压力\",\"columnId\":127,\"columnName\":\"pressure\",\"columnType\":\"int(6)\",\"createBy\":\"admin\",\"createTime\":\"2024-03-16 19:03:48\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"pressure\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":13,\"updateBy\":\"\",\"updateTime\":\"2024-03-16 19:05:14\",\"usableColumn\":false},{\"capJavaField\":\"Flow\",\"columnComment\":\"流量\",\"columnId\":128,\"columnName\":\"flow\",\"columnType\":\"int(6)\",\"createBy\":\"admin\",\"createTime\":\"2024-03-16 19:03:48\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:05:37');
INSERT INTO `sys_oper_log` VALUES (11, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"数据指标\",\"dictType\":\"data_index\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:08:15');
INSERT INTO `sys_oper_log` VALUES (12, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"正常\",\"dictSort\":0,\"dictType\":\"data_index\",\"dictValue\":\"1\",\"listClass\":\"success\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:08:55');
INSERT INTO `sys_oper_log` VALUES (13, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2024-03-16 19:08:55\",\"default\":false,\"dictCode\":120,\"dictLabel\":\"正常\",\"dictSort\":0,\"dictType\":\"data_index\",\"dictValue\":\"10\",\"isDefault\":\"N\",\"listClass\":\"warning\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:12:23');
INSERT INTO `sys_oper_log` VALUES (14, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2024-03-16 19:08:55\",\"default\":false,\"dictCode\":121,\"dictLabel\":\"一级错误\",\"dictSort\":0,\"dictType\":\"data_index\",\"dictValue\":\"11\",\"isDefault\":\"N\",\"listClass\":\"warning\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:12:30');
INSERT INTO `sys_oper_log` VALUES (15, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2024-03-16 19:08:55\",\"default\":false,\"dictCode\":122,\"dictLabel\":\"一级错误\",\"dictSort\":0,\"dictType\":\"data_index\",\"dictValue\":\"12\",\"isDefault\":\"N\",\"listClass\":\"warning\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:12:34');
INSERT INTO `sys_oper_log` VALUES (16, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2024-03-16 19:08:55\",\"default\":false,\"dictCode\":123,\"dictLabel\":\"一级错误\",\"dictSort\":0,\"dictType\":\"data_index\",\"dictValue\":\"13\",\"isDefault\":\"N\",\"listClass\":\"warning\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:12:39');
INSERT INTO `sys_oper_log` VALUES (17, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2024-03-16 19:08:55\",\"default\":false,\"dictCode\":124,\"dictLabel\":\"一级错误\",\"dictSort\":0,\"dictType\":\"data_index\",\"dictValue\":\"14\",\"isDefault\":\"N\",\"listClass\":\"danger\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:12:43');
INSERT INTO `sys_oper_log` VALUES (18, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2024-03-16 19:08:55\",\"default\":false,\"dictCode\":124,\"dictLabel\":\"一级错误\",\"dictSort\":0,\"dictType\":\"data_index\",\"dictValue\":\"14\",\"isDefault\":\"N\",\"listClass\":\"warning\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:12:47');
INSERT INTO `sys_oper_log` VALUES (19, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2024-03-16 19:08:55\",\"default\":false,\"dictCode\":125,\"dictLabel\":\"一级错误\",\"dictSort\":0,\"dictType\":\"data_index\",\"dictValue\":\"15\",\"isDefault\":\"N\",\"listClass\":\"warning\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:12:52');
INSERT INTO `sys_oper_log` VALUES (20, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2024-03-16 19:08:55\",\"default\":false,\"dictCode\":126,\"dictLabel\":\"二级错误\",\"dictSort\":0,\"dictType\":\"data_index\",\"dictValue\":\"16\",\"isDefault\":\"N\",\"listClass\":\"danger\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:12:57');
INSERT INTO `sys_oper_log` VALUES (21, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"monitorData\",\"className\":\"MonitorWaterfloodLineData\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"主键\",\"columnId\":125,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2024-03-16 19:03:48\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":13,\"updateBy\":\"\",\"updateTime\":\"2024-03-16 19:05:37\",\"usableColumn\":false},{\"capJavaField\":\"Temperature\",\"columnComment\":\"温度值\",\"columnId\":126,\"columnName\":\"temperature\",\"columnType\":\"int(6)\",\"createBy\":\"admin\",\"createTime\":\"2024-03-16 19:03:48\",\"dictType\":\"data_index\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"temperature\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":13,\"updateBy\":\"\",\"updateTime\":\"2024-03-16 19:05:37\",\"usableColumn\":false},{\"capJavaField\":\"Pressure\",\"columnComment\":\"压力\",\"columnId\":127,\"columnName\":\"pressure\",\"columnType\":\"int(6)\",\"createBy\":\"admin\",\"createTime\":\"2024-03-16 19:03:48\",\"dictType\":\"data_index\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"pressure\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":13,\"updateBy\":\"\",\"updateTime\":\"2024-03-16 19:05:37\",\"usableColumn\":false},{\"capJavaField\":\"Flow\",\"columnComment\":\"流量\",\"columnId\":128,\"columnName\":\"flow\",\"columnType\":\"int(6)\",\"createBy\":\"admin\",\"createTime\":\"2024-03-16 19:03:48\",\"dictType\":\"data_index\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:13:37');
INSERT INTO `sys_oper_log` VALUES (22, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"monitor_waterflood_line_data\"}', NULL, 0, NULL, '2024-03-16 19:14:28');
INSERT INTO `sys_oper_log` VALUES (23, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2024-03-07 16:29:18\",\"icon\":\"monitor\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2000,\"menuName\":\"数据监测管理\",\"menuType\":\"M\",\"orderNum\":0,\"params\":{},\"parentId\":0,\"path\":\"monitorData\",\"perms\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:20:45');
INSERT INTO `sys_oper_log` VALUES (24, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"monitorData/monitorData/index\",\"createTime\":\"2024-03-16 19:15:26\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2037,\"menuName\":\"注水管线数据\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2000,\"path\":\"monitorData\",\"perms\":\"monitor:monitorData:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:21:01');
INSERT INTO `sys_oper_log` VALUES (25, '部门管理', 1, 'com.ruoyi.web.controller.system.SysDeptController.add()', 'POST', 1, 'admin', NULL, '/system/dept', '127.0.0.1', '内网IP', '{\"ancestors\":\"0,100\",\"children\":[],\"createBy\":\"admin\",\"deptName\":\"A区域\",\"leader\":\"张三\",\"orderNum\":0,\"params\":{},\"parentId\":100,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:25:25');
INSERT INTO `sys_oper_log` VALUES (26, '部门管理', 1, 'com.ruoyi.web.controller.system.SysDeptController.add()', 'POST', 1, 'admin', NULL, '/system/dept', '127.0.0.1', '内网IP', '{\"ancestors\":\"0,100\",\"children\":[],\"createBy\":\"admin\",\"deptName\":\"B区域\",\"orderNum\":1,\"params\":{},\"parentId\":100,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:25:49');
INSERT INTO `sys_oper_log` VALUES (27, '用户管理', 3, 'com.ruoyi.web.controller.system.SysUserController.remove()', 'DELETE', 1, 'admin', NULL, '/system/user/100', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:25:57');
INSERT INTO `sys_oper_log` VALUES (28, '用户管理', 1, 'com.ruoyi.web.controller.system.SysUserController.add()', 'POST', 1, 'admin', NULL, '/system/user', '127.0.0.1', '内网IP', '{\"admin\":false,\"createBy\":\"admin\",\"deptId\":101,\"nickName\":\"A区域负责人\",\"params\":{},\"postIds\":[],\"roleIds\":[],\"status\":\"0\",\"userId\":101,\"userName\":\"testa\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:26:54');
INSERT INTO `sys_oper_log` VALUES (29, '用户管理', 1, 'com.ruoyi.web.controller.system.SysUserController.add()', 'POST', 1, 'admin', NULL, '/system/user', '127.0.0.1', '内网IP', '{\"admin\":false,\"createBy\":\"admin\",\"deptId\":102,\"nickName\":\"B区域负责人\",\"params\":{},\"postIds\":[],\"roleIds\":[],\"status\":\"0\",\"userId\":102,\"userName\":\"testb\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:27:21');
INSERT INTO `sys_oper_log` VALUES (30, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{\"admin\":false,\"createTime\":\"2024-03-08 18:25:26\",\"dataScope\":\"1\",\"delFlag\":\"0\",\"deptCheckStrictly\":true,\"flag\":false,\"menuCheckStrictly\":true,\"menuIds\":[2000,2037,2038,2039,2040,2041,2042],\"params\":{},\"roleId\":100,\"roleKey\":\"area_user\",\"roleName\":\"区域负责人\",\"roleSort\":0,\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:28:51');
INSERT INTO `sys_oper_log` VALUES (31, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"monitorData/monitorData/index\",\"createTime\":\"2024-03-16 19:15:26\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2037,\"menuName\":\"整体数据观测模块\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2000,\"path\":\"monitorData\",\"perms\":\"monitor:monitorData:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:29:37');
INSERT INTO `sys_oper_log` VALUES (32, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"monitorData/monitorData/index\",\"createTime\":\"2024-03-16 19:15:26\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2037,\"menuName\":\"整体数据观测模块\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2000,\"path\":\"allMonitorData\",\"perms\":\"monitor:monitorData:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:32:53');
INSERT INTO `sys_oper_log` VALUES (33, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"monitorData/monitorData/index\",\"createTime\":\"2024-03-16 19:15:26\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2043,\"menuName\":\"报警数据观测模块\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2000,\"path\":\"areaMonitorData\",\"perms\":\"monitor:monitorData:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:33:02');
INSERT INTO `sys_oper_log` VALUES (34, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', NULL, '/tool/gen/synchDb/monitor_waterflood_line_data', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:38:13');
INSERT INTO `sys_oper_log` VALUES (35, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"monitorData\",\"className\":\"MonitorWaterfloodLineData\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"主键\",\"columnId\":125,\"columnName\":\"id\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"createTime\":\"2024-03-16 19:03:48\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":13,\"updateBy\":\"\",\"updateTime\":\"2024-03-16 19:38:13\",\"usableColumn\":false},{\"capJavaField\":\"Temperature\",\"columnComment\":\"温度值\",\"columnId\":126,\"columnName\":\"temperature\",\"columnType\":\"int(6)\",\"createBy\":\"admin\",\"createTime\":\"2024-03-16 19:03:48\",\"dictType\":\"data_index\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"temperature\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":13,\"updateBy\":\"\",\"updateTime\":\"2024-03-16 19:38:13\",\"usableColumn\":false},{\"capJavaField\":\"Pressure\",\"columnComment\":\"压力\",\"columnId\":127,\"columnName\":\"pressure\",\"columnType\":\"int(6)\",\"createBy\":\"admin\",\"createTime\":\"2024-03-16 19:03:48\",\"dictType\":\"data_index\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"javaField\":\"pressure\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":13,\"updateBy\":\"\",\"updateTime\":\"2024-03-16 19:38:13\",\"usableColumn\":false},{\"capJavaField\":\"Flow\",\"columnComment\":\"流量\",\"columnId\":128,\"columnName\":\"flow\",\"columnType\":\"int(6)\",\"createBy\":\"admin\",\"createTime\":\"2024-03-16 19:03:48\",\"dictType\":\"data_index\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"is', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 19:42:16');
INSERT INTO `sys_oper_log` VALUES (36, '注水管线数据', 1, 'com.ruoyi.monitor.controller.MonitorWaterfloodLineDataController.add()', 'POST', 1, 'admin', NULL, '/monitor/monitorData', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2024-03-16 19:59:19\",\"deptId\":100,\"deptName\":\"智慧物流数据中心\",\"flow\":\"16\",\"id\":1768970295734968322,\"params\":{},\"pressure\":\"11\",\"temperature\":\"1\",\"warningMsg\":\"\"}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: Unknown column \'update_by\' in \'field list\'\r\n### The error may exist in com/ruoyi/monitor/mapper/MonitorWaterfloodLineDataMapper.java (best guess)\r\n### The error may involve com.ruoyi.monitor.mapper.MonitorWaterfloodLineDataMapper.insert-Inline\r\n### The error occurred while setting parameters\r\n### SQL: INSERT INTO monitor_waterflood_line_data  ( id, temperature, pressure, flow, dept_id, dept_name, warning_msg,   create_by, create_time, update_by, update_time )  VALUES  ( ?, ?, ?, ?, ?, ?, ?,   ?, ?, ?, ? )\r\n### Cause: java.sql.SQLSyntaxErrorException: Unknown column \'update_by\' in \'field list\'\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: Unknown column \'update_by\' in \'field list\'', '2024-03-16 20:00:06');
INSERT INTO `sys_oper_log` VALUES (37, '注水管线数据', 1, 'com.ruoyi.monitor.controller.MonitorWaterfloodLineDataController.add()', 'POST', 1, 'admin', NULL, '/monitor/monitorData', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2024-03-16 19:59:23\",\"deptId\":100,\"deptName\":\"智慧物流数据中心\",\"flow\":\"16\",\"id\":1768970313267159042,\"params\":{},\"pressure\":\"11\",\"temperature\":\"1\",\"warningMsg\":\"\"}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: Unknown column \'update_by\' in \'field list\'\r\n### The error may exist in com/ruoyi/monitor/mapper/MonitorWaterfloodLineDataMapper.java (best guess)\r\n### The error may involve com.ruoyi.monitor.mapper.MonitorWaterfloodLineDataMapper.insert-Inline\r\n### The error occurred while setting parameters\r\n### SQL: INSERT INTO monitor_waterflood_line_data  ( id, temperature, pressure, flow, dept_id, dept_name, warning_msg,   create_by, create_time, update_by, update_time )  VALUES  ( ?, ?, ?, ?, ?, ?, ?,   ?, ?, ?, ? )\r\n### Cause: java.sql.SQLSyntaxErrorException: Unknown column \'update_by\' in \'field list\'\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: Unknown column \'update_by\' in \'field list\'', '2024-03-16 20:00:10');
INSERT INTO `sys_oper_log` VALUES (38, '注水管线数据', 1, 'com.ruoyi.monitor.controller.MonitorWaterfloodLineDataController.add()', 'POST', 1, 'admin', NULL, '/monitor/monitorData', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2024-03-16 20:01:42\",\"deleted\":0,\"deptId\":100,\"deptName\":\"智慧物流数据中心\",\"flow\":\"16\",\"id\":1768970896975921153,\"params\":{},\"pressure\":\"11\",\"temperature\":\"1\",\"warningMsg\":\"\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 20:02:29');
INSERT INTO `sys_oper_log` VALUES (39, '注水管线数据', 1, 'com.ruoyi.monitor.controller.MonitorWaterfloodLineDataController.add()', 'POST', 1, 'admin', NULL, '/monitor/monitorData', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2024-03-16 20:03:00\",\"deleted\":0,\"deptId\":101,\"deptName\":\"A区域\",\"flow\":\"16\",\"id\":1768971225012436994,\"params\":{},\"pressure\":\"11\",\"temperature\":\"1\",\"warningMsg\":\"\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 20:03:48');
INSERT INTO `sys_oper_log` VALUES (40, '注水管线数据', 1, 'com.ruoyi.monitor.controller.MonitorWaterfloodLineDataController.add()', 'POST', 1, 'admin', NULL, '/monitor/monitorData', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2024-03-16 20:30:04\",\"deleted\":0,\"deptId\":101,\"deptName\":\"A区域\",\"flow\":\"16\",\"id\":1768978036755066882,\"maxValue\":1,\"params\":{},\"pressure\":\"11\",\"temperature\":\"1\",\"warningMsg\":\"\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 20:30:52');
INSERT INTO `sys_oper_log` VALUES (41, '注水管线数据', 1, 'com.ruoyi.monitor.controller.MonitorWaterfloodLineDataController.add()', 'POST', 1, 'admin', NULL, '/monitor/monitorData', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2024-03-16 20:31:22\",\"deleted\":0,\"deptId\":101,\"deptName\":\"A区域\",\"flow\":\"16\",\"id\":1768978364971937793,\"maxValue\":1,\"params\":{},\"pressure\":\"11\",\"temperature\":\"1\",\"warningMsg\":\"\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 20:32:10');
INSERT INTO `sys_oper_log` VALUES (42, '注水管线数据', 1, 'com.ruoyi.monitor.controller.MonitorWaterfloodLineDataController.add()', 'POST', 1, 'admin', NULL, '/monitor/monitorData', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2024-03-16 20:32:44\",\"deleted\":0,\"deptId\":101,\"deptName\":\"A区域\",\"flow\":\"16\",\"id\":1768978705121570817,\"maxValue\":16,\"params\":{},\"pressure\":\"11\",\"temperature\":\"1\",\"warningMsg\":\"\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 20:33:31');
INSERT INTO `sys_oper_log` VALUES (43, '注水管线数据', 1, 'com.ruoyi.monitor.controller.MonitorWaterfloodLineDataController.add()', 'POST', 1, 'admin', NULL, '/monitor/monitorData', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2024-03-16 20:34:06\",\"deleted\":0,\"deptId\":101,\"deptName\":\"A区域\",\"flow\":\"16\",\"id\":1768979051491385346,\"maxValue\":16,\"params\":{},\"pressure\":\"11\",\"temperature\":\"1\",\"warningMsg\":\"流量二级错误。\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 20:34:54');
INSERT INTO `sys_oper_log` VALUES (44, '注水管线数据', 1, 'com.ruoyi.monitor.controller.MonitorWaterfloodLineDataController.add()', 'POST', 1, 'admin', NULL, '/monitor/monitorData', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2024-03-16 20:35:19\",\"deleted\":0,\"deptId\":101,\"deptName\":\"A区域\",\"flow\":\"16\",\"id\":1768979356312408065,\"maxValue\":16,\"params\":{},\"pressure\":\"11\",\"temperature\":\"1\",\"warningMsg\":\"压力一级错误。流量二级错误。\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 20:36:06');
INSERT INTO `sys_oper_log` VALUES (45, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"monitorData/thisMonitorData/index\",\"createTime\":\"2024-03-16 19:15:26\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2043,\"menuName\":\"报警数据观测模块\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2000,\"path\":\"areaMonitorData\",\"perms\":\"monitor:monitorData:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 20:54:05');
INSERT INTO `sys_oper_log` VALUES (46, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.edit()', 'PUT', 1, 'admin', NULL, '/monitor/job', '127.0.0.1', '内网IP', '{\"concurrent\":\"1\",\"createBy\":\"admin\",\"createTime\":\"2024-03-06 18:45:20\",\"cronExpression\":\"0 0/1 * * * ?\",\"invokeTarget\":\"ryTask.generatedData()\",\"jobGroup\":\"SYSTEM\",\"jobId\":100,\"jobName\":\"生成数据\",\"misfirePolicy\":\"1\",\"nextValidTime\":\"2024-03-16 21:17:00\",\"params\":{},\"remark\":\"\",\"status\":\"1\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 21:17:38');
INSERT INTO `sys_oper_log` VALUES (47, '定时任务', 3, 'com.ruoyi.quartz.controller.SysJobController.remove()', 'DELETE', 1, 'admin', NULL, '/monitor/job/101', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 21:17:43');
INSERT INTO `sys_oper_log` VALUES (48, '定时任务', 3, 'com.ruoyi.quartz.controller.SysJobController.remove()', 'DELETE', 1, 'admin', NULL, '/monitor/job/102', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 21:17:45');
INSERT INTO `sys_oper_log` VALUES (49, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/monitor/job/run', '127.0.0.1', '内网IP', '{\"jobGroup\":\"SYSTEM\",\"jobId\":100,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 21:17:49');
INSERT INTO `sys_oper_log` VALUES (50, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/monitor/job/run', '127.0.0.1', '内网IP', '{\"jobGroup\":\"SYSTEM\",\"jobId\":100,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 21:19:28');
INSERT INTO `sys_oper_log` VALUES (51, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/monitor/job/run', '127.0.0.1', '内网IP', '{\"jobGroup\":\"SYSTEM\",\"jobId\":100,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 21:24:09');
INSERT INTO `sys_oper_log` VALUES (52, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.run()', 'PUT', 1, 'admin', NULL, '/monitor/job/run', '127.0.0.1', '内网IP', '{\"jobGroup\":\"SYSTEM\",\"jobId\":100,\"misfirePolicy\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 21:26:10');
INSERT INTO `sys_oper_log` VALUES (53, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.changeStatus()', 'PUT', 1, 'admin', NULL, '/monitor/job/changeStatus', '127.0.0.1', '内网IP', '{\"jobId\":100,\"misfirePolicy\":\"0\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 21:31:47');
INSERT INTO `sys_oper_log` VALUES (54, '注水管线数据', 3, 'com.ruoyi.monitor.controller.MonitorWaterfloodLineDataController.remove()', 'DELETE', 1, 'admin', NULL, '/monitor/monitorData/1768978705121570800', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2024-03-16 21:34:28');
INSERT INTO `sys_oper_log` VALUES (55, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.changeStatus()', 'PUT', 1, 'admin', NULL, '/monitor/job/changeStatus', '127.0.0.1', '内网IP', '{\"jobId\":100,\"misfirePolicy\":\"0\",\"params\":{},\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 23:05:36');
INSERT INTO `sys_oper_log` VALUES (56, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.edit()', 'PUT', 1, 'admin', NULL, '/monitor/job', '127.0.0.1', '内网IP', '{\"concurrent\":\"1\",\"createBy\":\"admin\",\"createTime\":\"2024-03-06 18:45:20\",\"cronExpression\":\"0 0/3 * * * ?\",\"invokeTarget\":\"ryTask.generatedData()\",\"jobGroup\":\"SYSTEM\",\"jobId\":100,\"jobName\":\"生成数据\",\"misfirePolicy\":\"1\",\"nextValidTime\":\"2024-03-16 23:09:00\",\"params\":{},\"remark\":\"\",\"status\":\"1\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 23:09:29');
INSERT INTO `sys_oper_log` VALUES (57, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.changeStatus()', 'PUT', 1, 'admin', NULL, '/monitor/job/changeStatus', '127.0.0.1', '内网IP', '{\"jobId\":100,\"misfirePolicy\":\"0\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 23:09:32');
INSERT INTO `sys_oper_log` VALUES (58, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2024-03-16 19:08:55\",\"default\":false,\"dictCode\":120,\"dictLabel\":\"正常\",\"dictSort\":0,\"dictType\":\"data_index\",\"dictValue\":\"10\",\"isDefault\":\"N\",\"listClass\":\"success\",\"params\":{},\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 23:32:28');
INSERT INTO `sys_oper_log` VALUES (59, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2024-03-06 09:31:40\",\"icon\":\"tool\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":3,\"menuName\":\"系统工具\",\"menuType\":\"M\",\"orderNum\":3,\"params\":{},\"parentId\":0,\"path\":\"tool\",\"perms\":\"\",\"query\":\"\",\"status\":\"1\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 23:52:37');
INSERT INTO `sys_oper_log` VALUES (60, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"system/post/index\",\"createTime\":\"2024-03-06 09:31:41\",\"icon\":\"post\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":104,\"menuName\":\"岗位管理\",\"menuType\":\"C\",\"orderNum\":5,\"params\":{},\"parentId\":1,\"path\":\"post\",\"perms\":\"system:post:list\",\"query\":\"\",\"status\":\"1\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 23:52:52');
INSERT INTO `sys_oper_log` VALUES (61, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"\",\"createTime\":\"2024-03-06 09:31:41\",\"icon\":\"log\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":108,\"menuName\":\"日志管理\",\"menuType\":\"M\",\"orderNum\":9,\"params\":{},\"parentId\":2000,\"path\":\"log\",\"perms\":\"\",\"query\":\"\",\"status\":\"1\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 23:53:23');
INSERT INTO `sys_oper_log` VALUES (62, '注水管线数据', 1, 'com.ruoyi.monitor.controller.MonitorWaterfloodLineDataController.add()', 'POST', 1, 'admin', NULL, '/monitor/monitorData', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2024-03-16 23:57:21\",\"deleted\":0,\"deptId\":101,\"deptName\":\"A区域\",\"flow\":\"10\",\"id\":1769030199896203266,\"maxValue\":16,\"params\":{},\"pressure\":\"16\",\"temperature\":\"1\",\"warningMsg\":\"压力二级错误。\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-16 23:58:08');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2024-03-06 09:31:40', '', NULL, '');
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2024-03-06 09:31:40', '', NULL, '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2024-03-06 09:31:40', '', NULL, '');
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2024-03-06 09:31:40', '', NULL, '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', 1, 1, '0', '0', 'admin', '2024-03-06 09:31:40', '', NULL, '超级管理员');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, '2', 1, 1, '0', '0', 'admin', '2024-03-06 09:31:40', '', NULL, '普通角色');
INSERT INTO `sys_role` VALUES (100, '区域负责人', 'area_user', 0, '1', 1, 1, '0', '0', 'admin', '2024-03-08 18:25:26', 'admin', '2024-03-16 19:28:51', NULL);

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 100);
INSERT INTO `sys_role_dept` VALUES (2, 101);
INSERT INTO `sys_role_dept` VALUES (2, 105);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 3);
INSERT INTO `sys_role_menu` VALUES (2, 4);
INSERT INTO `sys_role_menu` VALUES (2, 100);
INSERT INTO `sys_role_menu` VALUES (2, 101);
INSERT INTO `sys_role_menu` VALUES (2, 102);
INSERT INTO `sys_role_menu` VALUES (2, 103);
INSERT INTO `sys_role_menu` VALUES (2, 104);
INSERT INTO `sys_role_menu` VALUES (2, 105);
INSERT INTO `sys_role_menu` VALUES (2, 106);
INSERT INTO `sys_role_menu` VALUES (2, 107);
INSERT INTO `sys_role_menu` VALUES (2, 108);
INSERT INTO `sys_role_menu` VALUES (2, 109);
INSERT INTO `sys_role_menu` VALUES (2, 110);
INSERT INTO `sys_role_menu` VALUES (2, 111);
INSERT INTO `sys_role_menu` VALUES (2, 112);
INSERT INTO `sys_role_menu` VALUES (2, 113);
INSERT INTO `sys_role_menu` VALUES (2, 114);
INSERT INTO `sys_role_menu` VALUES (2, 115);
INSERT INTO `sys_role_menu` VALUES (2, 116);
INSERT INTO `sys_role_menu` VALUES (2, 117);
INSERT INTO `sys_role_menu` VALUES (2, 500);
INSERT INTO `sys_role_menu` VALUES (2, 501);
INSERT INTO `sys_role_menu` VALUES (2, 1000);
INSERT INTO `sys_role_menu` VALUES (2, 1001);
INSERT INTO `sys_role_menu` VALUES (2, 1002);
INSERT INTO `sys_role_menu` VALUES (2, 1003);
INSERT INTO `sys_role_menu` VALUES (2, 1004);
INSERT INTO `sys_role_menu` VALUES (2, 1005);
INSERT INTO `sys_role_menu` VALUES (2, 1006);
INSERT INTO `sys_role_menu` VALUES (2, 1007);
INSERT INTO `sys_role_menu` VALUES (2, 1008);
INSERT INTO `sys_role_menu` VALUES (2, 1009);
INSERT INTO `sys_role_menu` VALUES (2, 1010);
INSERT INTO `sys_role_menu` VALUES (2, 1011);
INSERT INTO `sys_role_menu` VALUES (2, 1012);
INSERT INTO `sys_role_menu` VALUES (2, 1013);
INSERT INTO `sys_role_menu` VALUES (2, 1014);
INSERT INTO `sys_role_menu` VALUES (2, 1015);
INSERT INTO `sys_role_menu` VALUES (2, 1016);
INSERT INTO `sys_role_menu` VALUES (2, 1017);
INSERT INTO `sys_role_menu` VALUES (2, 1018);
INSERT INTO `sys_role_menu` VALUES (2, 1019);
INSERT INTO `sys_role_menu` VALUES (2, 1020);
INSERT INTO `sys_role_menu` VALUES (2, 1021);
INSERT INTO `sys_role_menu` VALUES (2, 1022);
INSERT INTO `sys_role_menu` VALUES (2, 1023);
INSERT INTO `sys_role_menu` VALUES (2, 1024);
INSERT INTO `sys_role_menu` VALUES (2, 1025);
INSERT INTO `sys_role_menu` VALUES (2, 1026);
INSERT INTO `sys_role_menu` VALUES (2, 1027);
INSERT INTO `sys_role_menu` VALUES (2, 1028);
INSERT INTO `sys_role_menu` VALUES (2, 1029);
INSERT INTO `sys_role_menu` VALUES (2, 1030);
INSERT INTO `sys_role_menu` VALUES (2, 1031);
INSERT INTO `sys_role_menu` VALUES (2, 1032);
INSERT INTO `sys_role_menu` VALUES (2, 1033);
INSERT INTO `sys_role_menu` VALUES (2, 1034);
INSERT INTO `sys_role_menu` VALUES (2, 1035);
INSERT INTO `sys_role_menu` VALUES (2, 1036);
INSERT INTO `sys_role_menu` VALUES (2, 1037);
INSERT INTO `sys_role_menu` VALUES (2, 1038);
INSERT INTO `sys_role_menu` VALUES (2, 1039);
INSERT INTO `sys_role_menu` VALUES (2, 1040);
INSERT INTO `sys_role_menu` VALUES (2, 1041);
INSERT INTO `sys_role_menu` VALUES (2, 1042);
INSERT INTO `sys_role_menu` VALUES (2, 1043);
INSERT INTO `sys_role_menu` VALUES (2, 1044);
INSERT INTO `sys_role_menu` VALUES (2, 1045);
INSERT INTO `sys_role_menu` VALUES (2, 1046);
INSERT INTO `sys_role_menu` VALUES (2, 1047);
INSERT INTO `sys_role_menu` VALUES (2, 1048);
INSERT INTO `sys_role_menu` VALUES (2, 1049);
INSERT INTO `sys_role_menu` VALUES (2, 1050);
INSERT INTO `sys_role_menu` VALUES (2, 1051);
INSERT INTO `sys_role_menu` VALUES (2, 1052);
INSERT INTO `sys_role_menu` VALUES (2, 1053);
INSERT INTO `sys_role_menu` VALUES (2, 1054);
INSERT INTO `sys_role_menu` VALUES (2, 1055);
INSERT INTO `sys_role_menu` VALUES (2, 1056);
INSERT INTO `sys_role_menu` VALUES (2, 1057);
INSERT INTO `sys_role_menu` VALUES (2, 1058);
INSERT INTO `sys_role_menu` VALUES (2, 1059);
INSERT INTO `sys_role_menu` VALUES (2, 1060);
INSERT INTO `sys_role_menu` VALUES (100, 2000);
INSERT INTO `sys_role_menu` VALUES (100, 2037);
INSERT INTO `sys_role_menu` VALUES (100, 2038);
INSERT INTO `sys_role_menu` VALUES (100, 2039);
INSERT INTO `sys_role_menu` VALUES (100, 2040);
INSERT INTO `sys_role_menu` VALUES (100, 2041);
INSERT INTO `sys_role_menu` VALUES (100, 2042);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '密码',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 103 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 101, 'admin', '管理员', '00', '管理员@163.com', '15888888888', '1', '/profile/avatar/2024/03/08/blob_20240308225946A001.png', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2024-03-16 23:53:34', 'admin', '2024-03-06 09:31:40', '', '2024-03-16 23:54:21', '管理员');
INSERT INTO `sys_user` VALUES (101, 101, 'testa', 'A区域负责人', '00', '', '', '0', '', '$2a$10$c7Bg0a6f7hYTW.Q14.ermug6yr0tHnwEDIHLd3skwpIoic313/JpO', '0', '0', '', NULL, 'admin', '2024-03-16 19:26:54', '', NULL, NULL);
INSERT INTO `sys_user` VALUES (102, 102, 'testb', 'B区域负责人', '00', '', '', '0', '', '$2a$10$3qULXjq3fPSekSIUiO6jb.ZrKfYnZCLTrDBd38/J7LyhewQcs2Aci', '0', '0', '', NULL, 'admin', '2024-03-16 19:27:21', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);

SET FOREIGN_KEY_CHECKS = 1;
