package com.ruoyi.monitor.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.monitor.domain.MonitorWaterfloodLineData;

/**
 * 注水管线数据Mapper接口
 *
 * @author kf
 * @date 2024-03-16
 */
public interface MonitorWaterfloodLineDataMapper extends BaseMapper<MonitorWaterfloodLineData> {


    /**
     * 按照类型统计时间和数值
     * @return
     */
    List<Map<String, Object>> countGroupByType(String type);
}
