package com.ruoyi.monitor.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.mapper.SysDeptMapper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.monitor.domain.MonitorWaterfloodLineData;
import com.ruoyi.monitor.service.IMonitorWaterfloodLineDataService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 注水管线数据Controller
 *
 * @author kf
 * @date 2024-03-16
 */
@RestController
@RequestMapping("/monitor/monitorData")
public class MonitorWaterfloodLineDataController extends BaseController {
    @Autowired
    private IMonitorWaterfloodLineDataService monitorWaterfloodLineDataService;

    @Autowired
    private SysDeptMapper sysDeptMapper;


    /**
     * 按照类型统计时间和数值
     *
     * @return
     */
    @PreAuthorize("@ss.hasPermi('monitor:monitorData:list')")
    @GetMapping("/countGroupByType")
    public AjaxResult countGroupByType(String type) {
        return AjaxResult.success(monitorWaterfloodLineDataService.countGroupByType(type));
    }

    /**
     * 获取警告
     */
    @PreAuthorize("@ss.hasPermi('monitor:monitorData:list')")
    @GetMapping("/listWarning")
    public AjaxResult listWarning(MonitorWaterfloodLineData monitorWaterfloodLineData) {
        List<MonitorWaterfloodLineData> list = monitorWaterfloodLineDataService
                .list(new LambdaQueryWrapper<MonitorWaterfloodLineData>()
                        .gt(MonitorWaterfloodLineData::getMaxValue, 10));
        String msg = "";
        for (MonitorWaterfloodLineData waterfloodLineData : list) {
            msg = msg.length() < waterfloodLineData.getWarningMsg().length() ? waterfloodLineData.getWarningMsg() : msg;
        }
        return AjaxResult.success(msg);
    }

    /**
     * 获取统计数据图
     */
    @PreAuthorize("@ss.hasPermi('monitor:monitorData:list')")
    @GetMapping("/countGroup")
    public AjaxResult countGroup(MonitorWaterfloodLineData monitorWaterfloodLineData) {
        int count = monitorWaterfloodLineDataService
                .count(new LambdaQueryWrapper<MonitorWaterfloodLineData>()
                        .gt(MonitorWaterfloodLineData::getMaxValue, 10));
        return AjaxResult.success(count);
    }

    /**
     * 查询注水管线数据列表
     */
    @PreAuthorize("@ss.hasPermi('monitor:monitorData:list')")
    @GetMapping("/list")
    public TableDataInfo list(MonitorWaterfloodLineData monitorWaterfloodLineData) {
        startPage();
        List<MonitorWaterfloodLineData> list = monitorWaterfloodLineDataService
                .list(new LambdaQueryWrapper<MonitorWaterfloodLineData>()
                        .orderByDesc(MonitorWaterfloodLineData::getMaxValue)
                        .orderByDesc(MonitorWaterfloodLineData::getCreateTime));
        return getDataTable(list);
    }

    /**
     * 查询注水管线数据列表
     */
    @PreAuthorize("@ss.hasPermi('monitor:monitorData:list')")
    @GetMapping("/listArea")
    public TableDataInfo listArea(MonitorWaterfloodLineData monitorWaterfloodLineData) {
        startPage();
        List<MonitorWaterfloodLineData> list = monitorWaterfloodLineDataService
                .list(new LambdaQueryWrapper<MonitorWaterfloodLineData>()
                        .eq(MonitorWaterfloodLineData::getDeptId, SecurityUtils.getDeptId())
                        .orderByDesc(MonitorWaterfloodLineData::getMaxValue)
                        .orderByDesc(MonitorWaterfloodLineData::getCreateTime));
        return getDataTable(list);
    }


    /**
     * 导出注水管线数据列表
     */
    @PreAuthorize("@ss.hasPermi('monitor:monitorData:export')")
    @Log(title = "注水管线数据", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MonitorWaterfloodLineData monitorWaterfloodLineData) {
        List<MonitorWaterfloodLineData> list = monitorWaterfloodLineDataService.list();
        ExcelUtil<MonitorWaterfloodLineData> util = new ExcelUtil<MonitorWaterfloodLineData>(MonitorWaterfloodLineData.class);
        util.exportExcel(response, list, "注水管线数据数据");
    }

    /**
     * 获取注水管线数据详细信息
     */
    @PreAuthorize("@ss.hasPermi('monitor:monitorData:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(monitorWaterfloodLineDataService.getById(id));
    }

    /**
     * 新增注水管线数据
     */
    @PreAuthorize("@ss.hasPermi('monitor:monitorData:add')")
    @Log(title = "注水管线数据", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MonitorWaterfloodLineData monitorWaterfloodLineData) {
        return toAjax(monitorWaterfloodLineDataService.save(monitorWaterfloodLineData));
    }

    /**
     * 修改注水管线数据
     */
    @PreAuthorize("@ss.hasPermi('monitor:monitorData:edit')")
    @Log(title = "注水管线数据", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MonitorWaterfloodLineData monitorWaterfloodLineData) {
        return toAjax(monitorWaterfloodLineDataService.updateById(monitorWaterfloodLineData));
    }

    /**
     * 删除注水管线数据
     */
    @PreAuthorize("@ss.hasPermi('monitor:monitorData:remove')")
    @Log(title = "注水管线数据", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(monitorWaterfloodLineDataService.removeByIds(Arrays.asList(ids)));
    }
}
