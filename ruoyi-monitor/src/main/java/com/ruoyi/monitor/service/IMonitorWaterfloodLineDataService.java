package com.ruoyi.monitor.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.monitor.domain.MonitorWaterfloodLineData;
import com.baomidou.mybatisplus.extension.service.IService;
/**
 * 注水管线数据Service接口
 * 
 * @author kf
 * @date 2024-03-16
 */
public interface IMonitorWaterfloodLineDataService extends IService<MonitorWaterfloodLineData>
{
    /**
     * 按照类型统计时间和数值
     * @return
     */
    Map<String,Object> countGroupByType(String type);
}
