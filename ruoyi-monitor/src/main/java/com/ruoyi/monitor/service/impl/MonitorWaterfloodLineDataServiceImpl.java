package com.ruoyi.monitor.service.impl;

import java.util.*;

import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.service.ISysDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.monitor.mapper.MonitorWaterfloodLineDataMapper;
import com.ruoyi.monitor.domain.MonitorWaterfloodLineData;
import com.ruoyi.monitor.service.IMonitorWaterfloodLineDataService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * 注水管线数据Service业务层处理
 *
 * @author kf
 * @date 2024-03-16
 */
@Service
public class MonitorWaterfloodLineDataServiceImpl extends ServiceImpl<MonitorWaterfloodLineDataMapper, MonitorWaterfloodLineData> implements IMonitorWaterfloodLineDataService {


    @Autowired
    private MonitorWaterfloodLineDataMapper monitorWaterfloodLineDataMapper;
    @Autowired
    private ISysDeptService sysDeptService;

    /**
     * 按照类型统计时间和数值
     *
     * @return
     */
    @Override
    public Map<String,Object> countGroupByType(String type) {
        Map<String, Object> data = new HashMap<>();
        // 区域 时间值
        List<String> timeValues = new ArrayList<>();
        // 区域 实际值
        Map<String, List<Integer>> values = new HashMap<>();
        List<Map<String, Object>> maps = new ArrayList<>();

        List<Map<String, Object>> list = monitorWaterfloodLineDataMapper.countGroupByType(type);
        list.forEach(e -> {
//            List<String> timeValue = Optional.ofNullable(timeValues.get(e.get("deptName").toString())).orElse(new ArrayList<String>());
//            timeValue.add();
            timeValues.add(e.get("createTime").toString());
            List<Integer> value = Optional.ofNullable(values.get(e.get("deptName").toString())).orElse(new ArrayList<Integer>());
            value.add(Integer.parseInt(e.get("value").toString()));
            values.put(e.get("deptName").toString(),value);
        });

        List<String> area = new ArrayList<>();
        values.forEach((k, v) -> {
            Map<String, Object> dataMap = new HashMap<>();
            dataMap.put("name", k);
            dataMap.put("type", "line");
            dataMap.put("stack", "Total");
            dataMap.put("data", v);
            maps.add(dataMap);
            area.add(k);
        });
        data.put("time",timeValues);
        data.put("value",maps);
        Collections.reverse(area);
        data.put("area",area);
        return data;
    }

    @Override
    public boolean save(MonitorWaterfloodLineData entity) {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        SysDept sysDept = sysDeptService.selectDeptById(loginUser.getDeptId());
        entity.setDeptId(loginUser.getDeptId());
        entity.setDeptName(sysDept.getDeptName());
        // 温度
        int temperature = Integer.parseInt(entity.getTemperature());
        StringBuffer temperatureMsg = getWarningMsg(temperature, "温度");
        // 压力
        int pressure = Integer.parseInt(entity.getPressure());
        StringBuffer pressureMsg = getWarningMsg(pressure, "压力");
        // 流量
        int flow = Integer.parseInt(entity.getFlow());
        StringBuffer flowMsg = getWarningMsg(flow, "流量");
        entity.setWarningMsg(temperatureMsg.append(pressureMsg.toString()).append(flowMsg.toString()).toString());
        entity.setMaxValue(getMaxValue(temperature, pressure, flow));

        return baseMapper.insert(entity) > 0;
    }

    @Override
    public boolean updateById(MonitorWaterfloodLineData entity) {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        SysDept sysDept = sysDeptService.selectDeptById(loginUser.getDeptId());
        entity.setDeptId(loginUser.getDeptId());
        entity.setDeptName(sysDept.getDeptName());
        // 温度
        int temperature = Integer.parseInt(entity.getTemperature());
        StringBuffer temperatureMsg = getWarningMsg(temperature, "温度");
        // 压力
        int pressure = Integer.parseInt(entity.getPressure());
        StringBuffer pressureMsg = getWarningMsg(pressure, "压力");
        // 流量
        int flow = Integer.parseInt(entity.getFlow());
        StringBuffer flowMsg = getWarningMsg(flow, "流量");
        entity.setWarningMsg(temperatureMsg.append(pressureMsg.toString()).append(flowMsg.toString()).toString());
        entity.setMaxValue(getMaxValue(temperature, pressure, flow));

        return baseMapper.updateById(entity) > 0;
    }

    /**
     * 获取最大值
     *
     * @return
     */
    private int getMaxValue(int v1, int v2, int v3) {
        int v = Math.max(v1, v2);
        return Math.max(v3, v);
    }


    /**
     * 获取警告语
     *
     * @return
     */
    private StringBuffer getWarningMsg(int value, String tagget) {
        // 提示
        StringBuffer sb = new StringBuffer();
        if (value > 10 && value < 16) {
            sb.append(tagget).append("一级错误。");
        } else if (value > 15) {
            sb.append(tagget).append("二级错误。");
        }
        return sb;
    }


}
