package com.ruoyi.monitor.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 注水管线数据对象 monitor_waterflood_line_data
 *
 * @author kf
 * @date 2024-03-16
 */
@Data
public class MonitorWaterfloodLineData extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 温度值
     */
    @Excel(name = "温度值")
    private String temperature;

    /**
     * 压力
     */
    @Excel(name = "压力")
    private String pressure;

    /**
     * 流量
     */
    @Excel(name = "流量")
    private String flow;

    /**
     * 最大值
     */
    @Excel(name = "最大值")
    private Integer maxValue;

    /**
     * 所属区域id
     */
    @Excel(name = "所属区域id")
    private Long deptId;

    /**
     * 区域名称
     */
    @Excel(name = "区域名称")
    private String deptName;

    /**
     * 警告提示
     */
    @Excel(name = "警告提示")
    private String warningMsg;


    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public String getPressure() {
        return pressure;
    }

    public void setFlow(String flow) {
        this.flow = flow;
    }

    public String getFlow() {
        return flow;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setWarningMsg(String warningMsg) {
        this.warningMsg = warningMsg;
    }

    public String getWarningMsg() {
        return warningMsg;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("temperature", getTemperature())
                .append("pressure", getPressure())
                .append("flow", getFlow())
                .append("deptId", getDeptId())
                .append("deptName", getDeptName())
                .append("warningMsg", getWarningMsg())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .append("deleted", getDeleted())
                .toString();
    }
}
