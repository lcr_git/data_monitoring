package com.ruoyi.common.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.uuid.UUID;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * mybatisplus 填充器
 */
@Slf4j
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("start insert fill ....");
        Object createBy = metaObject.getValue("createBy");
//        String uuid = UUID.randomUUID().toString();
//        this.strictInsertFill(metaObject, "id", String.class, uuid); // 起始版本 3.3.0(推荐使用)
        if (StringUtils.isNull(createBy) || createBy.equals("")) {
            this.strictInsertFill(metaObject, "createBy", String.class, SecurityUtils.getUsername()); // 起始版本 3.3.0(推荐使用)
        }
        this.strictInsertFill(metaObject, "createTime", Date.class, DateUtils.getNowDate()); // 起始版本 3.3.0(推荐使用)
        this.strictInsertFill(metaObject, "deleted", Long.class, 0L); // 起始版本 3.3.0(推荐使用)

        // 或者
//        this.strictInsertFill(metaObject, "createTime", () -> LocalDateTime.now(), LocalDateTime.class); // 起始版本 3.3.3(推荐)
        // 或者
//        this.fillStrategy(metaObject, "createTime", LocalDateTime.now()); // 也可以使用(3.3.0 该方法有bug)
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("start update fill ....");
        Object updateBy = metaObject.getValue("updateBy");
        if (StringUtils.isNull(updateBy) || updateBy.equals("")) {
            this.strictInsertFill(metaObject, "updateBy", String.class, SecurityUtils.getUsername());
        }
        this.strictUpdateFill(metaObject, "updateTime", Date.class, DateUtils.getNowDate()); // 起始版本 3.3.0(推荐)
        // 或者
//        this.strictUpdateFill(metaObject, "updateTime", () -> LocalDateTime.now(), LocalDateTime.class); // 起始版本 3.3.3(推荐)
        // 或者
//        this.fillStrategy(metaObject, "updateTime", LocalDateTime.now()); // 也可以使用(3.3.0 该方法有bug)
    }
}
